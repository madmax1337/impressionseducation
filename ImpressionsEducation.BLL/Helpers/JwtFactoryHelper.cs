﻿using ImpressionsEducation.BLL.Models.Account.Response;
using ImpressionsEducation.Common.Options;
using ImpressionsEducation.Common.Security;
using ImpressionsEducation.DAL.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ImpressionsEducation.BLL.Helpers
{
    public class JwtFactoryHelper
    {
        public static JwtAuthenticationResponse GenerateToken(IOptions<AuthTokenProviderOption> tokenOptions, ApplicationUser user, string userRole)
        {
            DateTime now = DateTime.Now;
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenOptions?.Value?.JwtKey));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var accessTokenClaims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                new Claim(ProjectClaimsTypes.Identifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName.ToString()),
                new Claim(ClaimTypes.Role, userRole)
            };


            var refreshTokenClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                new Claim(ProjectClaimsTypes.Identifier,user.Id.ToString()),
                new Claim(ClaimTypes.Role, userRole)
            };

            DateTime accessTokenExpires = now.Add(TimeSpan.FromMinutes(tokenOptions.Value.AccessTokenExpiration));
            DateTime refreshTokenExpire = now.Add(TimeSpan.FromDays(tokenOptions.Value.RefreshTokenExpiration));

            JwtSecurityToken accessToken = GenerateToken(tokenOptions?.Value?.JwtIssuer, null, accessTokenExpires, accessTokenClaims, credentials);
            JwtSecurityToken refreshToken = GenerateToken(tokenOptions?.Value?.JwtIssuer, null, refreshTokenExpire, refreshTokenClaims, credentials);

            var encodedAccessToken = new JwtSecurityTokenHandler().WriteToken(accessToken);
            var encodedRefreshToken = new JwtSecurityTokenHandler().WriteToken(refreshToken);

            return new JwtAuthenticationResponse()
            {
                FullName = user.FirstName + " " + user.LastName,
                UserName = user.UserName,
                AccessToken = encodedAccessToken,
                RefreshToken = encodedRefreshToken
            };
        }

        private static JwtSecurityToken GenerateToken(string issuer, string audience, DateTime expiration, List<Claim> claims, SigningCredentials credentials)
        {
            return new JwtSecurityToken(
              issuer: issuer,
              audience: audience,
              claims: claims,
              expires: expiration,
              signingCredentials: credentials);
        }
    }
}
