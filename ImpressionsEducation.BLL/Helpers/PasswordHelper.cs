﻿using ImpressionsEducation.Common.Constants;
using System;
using System.Text;

namespace ImpressionsEducation.BLL.Helpers
{
    public class PasswordHelper
    {
        public static string GenerateNewPassword()
        {
            var charsetforcode = PasswordGeneration.CharsetForCode;
            var stringBuilder = new StringBuilder();
            var random = new Random();

            for (int i = 0; i < 14; i++)
            {
                stringBuilder.Append(charsetforcode[random.Next(charsetforcode.Length)]);
            }
            return stringBuilder.ToString();
        }
    }
}
