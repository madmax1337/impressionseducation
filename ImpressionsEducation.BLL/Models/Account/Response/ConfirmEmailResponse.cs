﻿namespace ImpressionsEducation.BLL.Models.Account.Response
{
    public class ConfirmEmailResponse: BaseResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
