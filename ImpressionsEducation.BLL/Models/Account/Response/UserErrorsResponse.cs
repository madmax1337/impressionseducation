﻿using ImpressionsEducation.Common.Enums;
using System.Collections.Generic;

namespace ImpressionsEducation.BLL.Models.Account.Response
{
    public class UserErrorsResponse : BaseResponse
    {
        public string UserId { get; set; }
        public List<UserErrorsItem> Errors { get; set; }
        public UserErrorsResponse()
        {
            Errors = new List<UserErrorsItem>();
        }
    }

    public class UserErrorsItem
    {
        public string Message { get; set; }
        public UserField UserField { get; set; }
    }
}
