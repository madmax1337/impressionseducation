﻿namespace ImpressionsEducation.BLL.Models.Account.Response
{
    public class JwtAuthenticationResponse: BaseResponse
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
