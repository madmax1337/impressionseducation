﻿namespace ImpressionsEducation.BLL.Models.Account.Request
{
    public class RefreshTokenAuthenticationRequest
    {
        public string RefreshToken { get; set; } 
    }
}
