﻿namespace ImpressionsEducation.BLL.Models.Account.Request
{
    public class SignInRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
