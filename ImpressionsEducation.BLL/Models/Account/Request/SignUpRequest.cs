﻿using ImpressionsEducation.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace ImpressionsEducation.BLL.Models.Account.Request
{
    public class SignUpRequest
    {
        [Required]
        [RegularExpression(Patterns.firstNamePattern, ErrorMessage = Messages.firstNameValidationError)]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(Patterns.lastNamePattern, ErrorMessage = Messages.lastNameValidationError)]
        public string LastName { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [RegularExpression(Patterns.emailPatern, ErrorMessage = Messages.incorrectEmailError)]
        public string Email { get; set; }

        [Required]
        [RegularExpression(Patterns.passwordPattern, ErrorMessage = Messages.passwordCharactersError)]
        public string Password { get; set; }

        public string Image { get; set; }
    }
}
