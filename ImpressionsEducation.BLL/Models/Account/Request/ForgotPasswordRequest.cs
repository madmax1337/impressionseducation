﻿namespace ImpressionsEducation.BLL.Models.Account.Request
{
    public class ForgotPasswordRequest
    {
        public string Email { get; set; }
    }
}
