﻿namespace ImpressionsEducation.BLL.Models.CategoryImpressions.Request
{
    public class UpdateCategoryImpressionsRequest
    {
        public string CategoryId { get; set; }
        public string NameCategory { get; set; }
        public bool IsRemoved { get; set; }
    }
}
