﻿namespace ImpressionsEducation.BLL.Models.CategoryImpressions.Request
{
    public class CreateCategoryImpressionsRequest
    {
        public string NameCategory { get; set; }
    }
}
