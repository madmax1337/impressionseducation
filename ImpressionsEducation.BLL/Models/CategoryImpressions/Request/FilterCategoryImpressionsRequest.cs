﻿using ImpressionsEducation.Common.Enums;

namespace ImpressionsEducation.BLL.Models.CategoryImpressions.Request
{
    public class FilterCategoryImpressionsRequest
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string NameCategory { get; set; }
        public SortOrder SortOrder { get; set; }
        public CategoryColumnName CategoryColumnName { get; set; }
    }
}
