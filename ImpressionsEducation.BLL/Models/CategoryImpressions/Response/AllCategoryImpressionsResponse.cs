﻿using System;
using System.Collections.Generic;

namespace ImpressionsEducation.BLL.Models.CategoryImpressions.Response
{
    public class AllCategoryImpressionsResponse: BaseResponse
    {
        public List<AllCategoryImpressionsItem> Items { get; set; }
        public int Total { get; set; }

        public AllCategoryImpressionsResponse()
        {
            Items = new List<AllCategoryImpressionsItem>();
        }
    }

    public class AllCategoryImpressionsItem
    {
        public string Id { get; set; }

        public DateTime CreationDate { get; set; }
        public string NameCattegory { get; set; }
    }
}
