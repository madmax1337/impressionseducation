﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionsEducation.BLL.Models.User.Request
{
    public class UpdateUserRequest
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string Image { get; set; }
        public bool IsRemove { get; set; }
    }
}
