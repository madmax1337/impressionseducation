﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ImpressionsEducation.BLL.Models.User.Request
{
    public class DisableEnableUserRequest
    {
        [Required]
        public string Id { get; set; }

        public bool IsDisable { get; set; }
    }
}
