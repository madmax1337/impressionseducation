﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionsEducation.BLL.Models.User.Response
{
    public class UserProfileResponse: BaseResponse
    {

        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
    }
}
