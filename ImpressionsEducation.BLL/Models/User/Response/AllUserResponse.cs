﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionsEducation.BLL.Models.User.Response
{
    public class AllUserResponse : BaseResponse
    {
        public List<AllUserItem> Items { get; set; }
        public int Total { get; set; }

        public AllUserResponse()
        {
            Items = new List<AllUserItem>();
        }
    }

    public class AllUserItem
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsDisabled { get; set; }
    }
}
