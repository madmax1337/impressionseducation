﻿
using ImpressionsEducation.Common.Enums;

namespace ImpressionsEducation.BLL.Models.Order.Request
{
    public class OrderFilterRequest
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string UserId { get; set; }
        public SortOrder SortOrder { get; set; }
        public OrderColumnName OrderColumnName { get; set; }
        public StatusOrder StatusOrder { get; set; }
    }
}
