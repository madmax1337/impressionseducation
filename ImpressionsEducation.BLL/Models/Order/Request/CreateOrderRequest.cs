﻿using System.Collections.Generic;

namespace ImpressionsEducation.BLL.Models.Order.Request
{
    public class CreateOrderRequest
    {
        public List<CartModelItem> Items { get; set; }
        public string TransactionId { get; set; }
        public decimal OrderAmount { get; set; }
    }

    public class CartModelItem
    {
        public string ImpressionsId { get; set; }
        public decimal Amount { get; set; }
        public int Count { get; set; }

    }
}
