﻿using ImpressionsEducation.Common.Enums;

namespace ImpressionsEducation.BLL.Models.Order.Request
{
    public class UpdateOrderRequest
    {
        public string OrderId { get; set; }
        public StatusOrder StatusOrder { get; set; }
    }
}
