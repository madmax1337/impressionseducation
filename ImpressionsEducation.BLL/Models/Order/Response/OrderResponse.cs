﻿using ImpressionsEducation.Common.Models.Order;
using System;
using System.Collections.Generic;

namespace ImpressionsEducation.BLL.Models.Order.Response
{
    public class OrderResponse: BaseResponse
    {
        public List<OrderResponseItem> Items { get; set; }
        public int Total { get; set; }

        public OrderResponse()
        {
            Items = new List<OrderResponseItem>();
        }
    }

    public class OrderResponseItem
    {
        public string OrderId { get; set; }
        public DateTime DateOrder { get; set; }
        public decimal OrderAmount { get; set; }
        public int OrderStatus { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public List<OrderItemData> OrderItems { get; set; }
    }
}
