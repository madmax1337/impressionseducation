﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionsEducation.BLL.Models.FeedBack.Response
{
    public class ComentaryWebsiteResponse: BaseResponse
    {
        public List<ComentaryWebsiteItem> Items { get; set; }
        public ComentaryWebsiteResponse()
        {
            Items = new List<ComentaryWebsiteItem>();
        }
    }

    public class ComentaryWebsiteItem
    {
        public DateTime DateCreate { get; set; }
        public string UserName { get; set; }
        public string Comentary { get; set; }
    }
}
