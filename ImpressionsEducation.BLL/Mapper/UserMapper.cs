﻿using AutoMapper;
using ImpressionsEducation.BLL.Models.User.Request;
using ImpressionsEducation.BLL.Models.User.Response;
using ImpressionsEducation.Common.Models.User;
using ImpressionsEducation.DAL.Entities;

namespace ImpressionsEducation.BLL.Mapper
{
    public class UserMapper: Profile
    {
        public UserMapper()
        {
            CreateMap<ApplicationUser, UserProfileResponse>();
            CreateMap<ApplicationUser, AllUserItem>();
            CreateMap<AllUserRequest, UserFilterModel>();
        }
    }
}
