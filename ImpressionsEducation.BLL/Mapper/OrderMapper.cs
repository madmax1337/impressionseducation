﻿using AutoMapper;
using ImpressionsEducation.BLL.Models.Order.Request;
using ImpressionsEducation.BLL.Models.Order.Response;
using ImpressionsEducation.Common.Models.Order;

namespace ImpressionsEducation.BLL.Mapper
{
    public class OrderMapper : Profile
    {
        public OrderMapper()
        {
            CreateMap<DataOrderItemModel, OrderResponseItem>();
            CreateMap<OrderFilterRequest, OrderFilterModel>();
        }
    }
}
