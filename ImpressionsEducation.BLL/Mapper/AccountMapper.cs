﻿using AutoMapper;
using ImpressionsEducation.BLL.Models.Account.Request;
using ImpressionsEducation.DAL.Entities;

namespace ImpressionsEducation.BLL.Mapper
{
    public class AccountMapper: Profile
    {
        public AccountMapper()
        {
            CreateMap<SignUpRequest, ApplicationUser>();
        }
    }
}
