﻿using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services
{
    public class LogExeptionService : ILogExceptionService
    {
        private readonly ILogExceptionRepository _logExceptionRepository;

        public LogExeptionService(ILogExceptionRepository logExceptionRepository)
        {
            _logExceptionRepository = logExceptionRepository;
        }

        public async Task Create(Exception exception, string url)
        {
            var exceptionToInsert = new LogException
            {
                Message = exception.InnerException != null ? exception.InnerException.Message : exception.Message,
                Request = url,
                Source = exception.Source,
                StackTrace = exception.InnerException != null ? exception.InnerException.StackTrace : exception.StackTrace
            };

            await _logExceptionRepository.Insert(exceptionToInsert);
        }
    }
}
