﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ImpressionsEducation.BLL.Helpers;
using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.BLL.Models.Account.Request;
using ImpressionsEducation.BLL.Models.Account.Response;
using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.BLL.Services.SubServices;
using ImpressionsEducation.Common.Constants;
using ImpressionsEducation.Common.Enums;
using ImpressionsEducation.Common.Exceptions;
using ImpressionsEducation.Common.Options;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using ImpressionsEducation.Common.Security;
using Microsoft.AspNetCore.Routing;

namespace ImpressionsEducation.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly IOptions<AuthTokenProviderOption> _tokenOptions;
        private readonly IUserRepository _userRepository;
        private readonly UserValidationService _userValidationService;
        private readonly EmailService _emailService;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly LinkGenerator _linkGenerator;

        public AccountService(IUserRepository userRepository,
            UserValidationService userValidationService,
            IOptions<AuthTokenProviderOption> tokenOptions,
            IMapper mapper,
            EmailService emailService,
            IHttpContextAccessor httpContextAccessor,
            LinkGenerator linkGenerator)
        {
            _userRepository = userRepository;
            _userValidationService = userValidationService;
            _tokenOptions = tokenOptions;
            _mapper = mapper;
            _emailService = emailService;
            _httpContextAccessor = httpContextAccessor;
            _linkGenerator = linkGenerator;
        }

        public async Task<BaseResponse> ConfirmEmailAsync(string userId, string code)
        {
            ConfirmEmailResponse responseModel = new ConfirmEmailResponse() { IsSuccessful = true };

            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
            {
                responseModel.Message = Messages.confirmEmailError;
                responseModel.IsSuccessful = false;
                return responseModel;
            }

            var user = await _userRepository.FindByIdAsync(userId);

            if (user.Equals(null))
            {
                responseModel.Message = Messages.userNotFound;
                responseModel.IsSuccessful = false;
                return responseModel;
            }

            var result = await _userRepository.ConfirmEmailAsync(user, code);

            if (!result.Succeeded)
            {
                responseModel.Message = result.Errors.Select(x => x.Description).FirstOrDefault();
                responseModel.IsSuccessful = false;
                return responseModel;
            }

            responseModel.FirstName = user.FirstName;
            responseModel.LastName = user.LastName;

            return responseModel;
        }

        private async Task<BaseResponse> EmailSenderAsync(string callbackUrl, string email, string code)
        {
            BaseResponse responseModel = new BaseResponse() { IsSuccessful = true };

            var msg = string.Empty;

            if (string.IsNullOrWhiteSpace(email))
            {
                responseModel.Message = Messages.emailSendError;
                responseModel.IsSuccessful = false;
                return responseModel;
            }

            msg = string.IsNullOrWhiteSpace(code) ? $"Confirm the registration by clicking on the link:<a href={callbackUrl}>link</a>" : $"Your new password: {code}";

            await _emailService.SendEmailAsync(email, Email.EmailConfirm, msg);

            return responseModel;
        }

        public async Task<BaseResponse> ForgotPasswordAsync(ForgotPasswordRequest requestModel)
        {
            BaseResponse responseModel = new BaseResponse() { IsSuccessful = true };

            var user = await _userRepository.FindByEmailAsync(requestModel.Email);

            if (user.Equals(null))

            {
                responseModel.Message = Messages.userNotFound;
                responseModel.IsSuccessful = false;
                return responseModel;
            }

            var code = PasswordHelper.GenerateNewPassword();
            var result = await _userRepository.ResetPasswordAsync(user, code);

            if (!result.Succeeded)
            {
                responseModel.Message = result.Errors.Select(x => x.Description).FirstOrDefault();
            }
            responseModel = await EmailSenderAsync(null, user.Email, code);
            return responseModel;
        }

        public async Task LogOutAsync()
        {
            await _userRepository.LogOutAsync();
        }

        public async Task<BaseResponse> SignInAsync(SignInRequest requestModel)
        {
            ApplicationUser user = await _userRepository.FindByEmailAsync(requestModel.Email);

            UserErrorsResponse userErrors = new UserErrorsResponse { IsSuccessful = false };

            await _userValidationService.UserIsExistOrDisapbledAsync(requestModel.Email, userErrors);

            if (userErrors.Errors.Any())
            {
                return userErrors;
            }

            await _userValidationService.CheckEmail(requestModel.Email, userErrors);

            if (userErrors.Errors.Any())
            {
                return userErrors;
            }

            _userValidationService.PasswordVerification(user, requestModel.Password, userErrors);

            if (userErrors.Errors.Any())
            {
                return userErrors;
            }

            var userRole = (await _userRepository.GetRoleAsync(user)).FirstOrDefault();

            JwtAuthenticationResponse jwtModel = JwtFactoryHelper.GenerateToken(_tokenOptions, user, userRole);

            jwtModel.IsSuccessful = true;

            return jwtModel;
        }

        public async Task<BaseResponse> SignUpAsync(SignUpRequest requestModel)
        {
            IdentityResult queryResult;
            ApplicationUser user = _mapper.Map<ApplicationUser>(requestModel);
            var userValidationView = await _userValidationService.CreateValidationAsync(user);

            if (userValidationView.IsSuccessful == false)
            {
                return userValidationView;
            }

            BaseResponse responseModel = new BaseResponse { IsSuccessful = true };

            user.Role = (int)Role.User;
            user.Password = _userRepository.HashPassword(user, user.Password);
            user.PasswordHash = user.Password;
            queryResult = await _userRepository.CreateAsync(user);
            _userValidationService.HadlingIdentityResult(queryResult);

            queryResult = await _userRepository.AddToRoleAsync(user, Role.User.ToString());
            _userValidationService.HadlingIdentityResult(queryResult);

            var code = await _userRepository.GenerateEmailTokenAsync(user);

            var callbackUrl = GenerateConfirmEmailLink(user.Id, code);

            responseModel = await EmailSenderAsync(callbackUrl, user.Email, string.Empty);

            return responseModel;
        }

        private string GenerateConfirmEmailLink(string userId, string code)
        {
            var callbackLink = _linkGenerator.GetUriByAction(_httpContextAccessor.HttpContext,
                action: "ConfirmEmail",
                controller: "Account",
                values: new { userId, code });

            return callbackLink;
        }


        public async Task<BaseResponse> RefreshToken(RefreshTokenAuthenticationRequest requestModel)
        {
            JwtSecurityToken refreshToken = new JwtSecurityTokenHandler().ReadJwtToken(requestModel.RefreshToken);

            if (refreshToken.ValidFrom >= DateTime.UtcNow || refreshToken.ValidTo <= DateTime.UtcNow)
            {
                throw new ProjectException(StatusCodes.Status401Unauthorized, Messages.refreshTokenExpired);
            }

            string userId = refreshToken.Claims.FirstOrDefault(x => x.Type == ProjectClaimsTypes.Identifier)?.Value;

            ApplicationUser user = await _userRepository.FindByIdAsync(userId);

            if (user == null)
            {
                throw new ProjectException(StatusCodes.Status404NotFound, Messages.userExistError);
            }

            string userRole = (await _userRepository.GetRoleAsync(user)).FirstOrDefault();

            JwtAuthenticationResponse jwtModel = JwtFactoryHelper.GenerateToken(_tokenOptions, user, userRole);

            jwtModel.IsSuccessful = true;

            return jwtModel;
        }
    }
}
