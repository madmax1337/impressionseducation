﻿using System;
using System.Threading.Tasks;
using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;

namespace ImpressionsEducation.BLL.Services
{
    public class PaymentService: IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;

        public PaymentService(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }

        public async Task<Payment> Create(string transactionId)
        {
            var payment = new Payment();

            if (string.IsNullOrWhiteSpace(transactionId))
            {
                return payment;
            }

            payment.TransactionId = transactionId;

            await _paymentRepository.Insert(payment);

            return payment;
        }
    }
}
