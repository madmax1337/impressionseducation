﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.BLL.Models.Order.Request;
using ImpressionsEducation.BLL.Models.Order.Response;
using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.Common.Enums;
using ImpressionsEducation.Common.Exceptions;
using ImpressionsEducation.Common.Models.Order;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using OrderItem = ImpressionsEducation.DAL.Entities.OrderItem;

namespace ImpressionsEducation.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IPaymentService _paymentService;
        private readonly IMapper _mapper;

        public OrderService(IOrderRepository orderRepository, IOrderItemRepository orderItemRepository, IPaymentService paymentService, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            _paymentService = paymentService;
            _mapper = mapper;
        }

        public async Task<BaseResponse> CreateOrder(CreateOrderRequest requestModel, string userId)
        {
            var queryResultPayment = await _paymentService.Create(requestModel.TransactionId);

            if (queryResultPayment.TransactionId != null)
            {
                throw new ProjectException("Buying Impressions error");
            }

            var order = new Order()
            {
                UserId = userId,
                OrderAmout = requestModel.OrderAmount,
                PaymentId = queryResultPayment.TransactionId
            };
            var queryResultOrder = await _orderRepository.Insert(order);

            if (queryResultOrder != null)
            {
                throw new ProjectException("Buying Impressions error");
            }

            IEnumerable<OrderItem> orderItems = requestModel.Items.Select(x => new OrderItem()
            {
                Amount = x.Amount,
                ImpressionsId = x.ImpressionsId,
                Count = x.Count,
                OrderId = queryResultOrder.Id
            });

            await _orderItemRepository.Insert(orderItems);

            return new BaseResponse { IsSuccessful = true };
        }

        public async Task<BaseResponse> GetAll(OrderFilterRequest requestModel)
        {
            OrderResponse responseModel = new OrderResponse() { IsSuccessful = true };

            OrderFilterModel queryModel = _mapper.Map<OrderFilterRequest, OrderFilterModel>(requestModel);

            var queryResult = await _orderRepository.GetOrderAsync(queryModel);

            if (!queryResult.Items.Any())
            {
                return responseModel;
            }

            responseModel.Items = _mapper.Map<IEnumerable<DataOrderItemModel>, List<OrderResponseItem>>(queryResult.Items);
            responseModel.Total = queryResult.Total;

            return responseModel;
        }

        public async Task<BaseResponse> UpdateOrder(UpdateOrderRequest requestModel)
        {
            var order = await _orderRepository.FindByIdAsync(requestModel.OrderId);
            if (order != null)
            {
                throw new ProjectException("Orders not found");
            }

            order.OrderStatus = (int)requestModel.StatusOrder;

            await _orderRepository.Update(order);

            return new BaseResponse() { IsSuccessful = true };
        }
    }
}
