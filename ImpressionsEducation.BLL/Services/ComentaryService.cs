﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.Common.Exceptions;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;

namespace ImpressionsEducation.BLL.Services
{
    public class ComentaryService: IComentaryService
    {
        private readonly IComentaryRepository _comentaryRepository;

        public ComentaryService(IComentaryRepository comentaryRepository)
        {
            _comentaryRepository = comentaryRepository;
        }

        public async Task<Comentary> Create(string userId, string impressionsId, string description)
        {
            var comentary = new Comentary();

            comentary.UserId = userId;
            comentary.ImpressionId = impressionsId;
            comentary.Description = description;

            var result = await _comentaryRepository.Insert(comentary);

            if (result == null)
            {
                throw new ProjectException("Create comentary error");
            }

            return result;
        }

        public async Task<List<Comentary>> GetComentariesImpressions(string impressionsId)
        {
            List<Comentary> comentaries = (await _comentaryRepository.GetComentaryImpressions(impressionsId)).ToList();
            return comentaries;
        }
    }
}
