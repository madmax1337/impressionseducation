﻿using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.DAL.Repository.Interfaces;

namespace ImpressionsEducation.BLL.Services
{
    public class ImpessionsService: IImpessionsService
    {
        private readonly IComentaryService _comentaryService;
        private readonly IImpressionRepository _impressionRepository;

        public ImpessionsService(IComentaryService comentaryService, IImpressionRepository impressionRepository)
        {
            _comentaryService = comentaryService;
            _impressionRepository = impressionRepository;
        }
    }
}
