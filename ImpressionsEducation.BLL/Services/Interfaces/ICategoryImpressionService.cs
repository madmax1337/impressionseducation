﻿using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.BLL.Models.CategoryImpressions.Request;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.Interfaces
{
    public interface ICategoryImpressionService
    {
        Task<BaseResponse> Create(CreateCategoryImpressionsRequest requestModel);
        Task<BaseResponse> Update(UpdateCategoryImpressionsRequest requestModel);
        Task<BaseResponse> GetAllCategory(FilterCategoryImpressionsRequest requestModel);
    }
}
