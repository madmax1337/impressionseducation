﻿using ImpressionsEducation.DAL.Entities;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.Interfaces
{
    public interface IPaymentService
    {
        Task<Payment> Create(string transactionId);
    }
}
