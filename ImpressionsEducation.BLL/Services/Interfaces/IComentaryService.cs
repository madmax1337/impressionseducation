﻿using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.Interfaces
{
    public interface IComentaryService
    {
        Task<Comentary> Create(string userId, string impressionsId, string description);
        Task<List<Comentary>> GetComentariesImpressions(string impressionsId);
    }
}
