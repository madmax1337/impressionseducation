﻿using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.BLL.Models.Order.Request;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.Interfaces
{
    public interface IOrderService
    {
        Task<BaseResponse> GetAll(OrderFilterRequest requestModel);
        Task<BaseResponse> CreateOrder(CreateOrderRequest requestModel, string userId);
        Task<BaseResponse> UpdateOrder(UpdateOrderRequest requestModel);
    }
}
