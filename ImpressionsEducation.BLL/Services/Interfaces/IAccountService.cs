﻿using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.BLL.Models.Account.Request;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.Interfaces
{
    public interface IAccountService
    {
        Task<BaseResponse> SignInAsync(SignInRequest requestModel);
        Task<BaseResponse> SignUpAsync(SignUpRequest requestModel);
        Task<BaseResponse> ForgotPasswordAsync(ForgotPasswordRequest requestModel);
        Task<BaseResponse> ConfirmEmailAsync(string userId, string code);
        Task LogOutAsync();

        Task<BaseResponse> RefreshToken(RefreshTokenAuthenticationRequest requestModel);
    }
}
