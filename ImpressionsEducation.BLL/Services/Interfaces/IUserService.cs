﻿using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.BLL.Models.User.Request;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.Interfaces
{
    public interface IUserService
    {
        Task<BaseResponse> GetUserProfile(ClaimsPrincipal claims);
        Task<BaseResponse> UpdateAsync(UpdateUserRequest requestModel);
        Task<BaseResponse> SetStatusAsync(DisableEnableUserRequest requestModel);
        Task<BaseResponse> GetAllAsync(AllUserRequest requestModel);
    }
}
