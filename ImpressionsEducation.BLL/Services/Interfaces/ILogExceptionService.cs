﻿using System;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.Interfaces
{
    public interface ILogExceptionService
    {
        Task Create(Exception exception, string url);
    }
}
