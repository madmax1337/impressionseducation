﻿using ImpressionsEducation.BLL.Models;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.Interfaces
{
    public interface IFeedBackWebsiteService
    {
        Task<BaseResponse> Create(ClaimsPrincipal claims, string descriptyon);
        Task<BaseResponse> GetCometary();
    }
}
