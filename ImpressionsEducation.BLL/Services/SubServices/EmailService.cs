﻿using ImpressionsEducation.Common.Constants;
using ImpressionsEducation.Common.Options;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.SubServices
{
    public class EmailService
    {
        private readonly EmailOption _emailOption;

        public EmailService(IOptions<EmailOption> emailOption)
        {
            _emailOption = emailOption.Value;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(Email.Name, _emailOption.Email));
            emailMessage.To.Add(new MailboxAddress(email, email));
            emailMessage.Subject = subject;

            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_emailOption.Smtp, _emailOption.Port, MailKit.Security.SecureSocketOptions.StartTls);
                await client.AuthenticateAsync(_emailOption.Email, _emailOption.Password);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }

        }
    }
}
