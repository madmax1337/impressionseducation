﻿using ImpressionsEducation.BLL.Models.Account.Response;
using ImpressionsEducation.Common.Constants;
using ImpressionsEducation.Common.Enums;
using ImpressionsEducation.Common.Exceptions;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services.SubServices
{
    public class UserValidationService
    {
        private readonly IUserRepository _userRepository;

        public UserValidationService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<UserErrorsResponse> CreateValidationAsync(ApplicationUser user)
        {
            var userErrors = new UserErrorsResponse() { IsSuccessful = true };

            FirstNameByPattern(user.FirstName, userErrors);

            LastNameByPattern(user.LastName, userErrors);

            await UserNameIsExistAsync(user, userErrors);

            await EmailIsExistAsync(user, userErrors);

            EmaiByPattern(user, userErrors);

            PasswordByPattern(user.Password, userErrors);

            if (userErrors.Errors.Any())
            {
                userErrors.IsSuccessful = false;
            }

            return userErrors;
        }

        public async Task<UserErrorsResponse> UpdateValidationAsync(ApplicationUser user)
        {
            bool updating = true;

            var userErrors = new UserErrorsResponse() { IsSuccessful = true };

            FirstNameByPattern(user.FirstName, userErrors);

            LastNameByPattern(user.LastName, userErrors);

            await UserNameIsExistAsync(user, userErrors, updating);

            await EmailIsExistAsync(user, userErrors, updating);

            EmaiByPattern(user, userErrors);

            if (userErrors.Errors.Any())
            {
                userErrors.IsSuccessful = false;
            }

            return userErrors;
        }

        public void FirstNameByPattern(string firstName, UserErrorsResponse userErrors)
        {
            if (!new Regex(Patterns.firstNamePattern).IsMatch(firstName))
            {
                var newError = new UserErrorsItem()
                {
                    UserField = UserField.FirstName,
                    Message = Messages.firstNameValidationError
                };
                userErrors.Errors.Add(newError);
            }
        }

        public void LastNameByPattern(string lastName, UserErrorsResponse userErrors)
        {
            if (!new Regex(Patterns.lastNamePattern).IsMatch(lastName))
            {
                var newError = new UserErrorsItem
                {
                    UserField = UserField.LastName,
                    Message = Messages.lastNameValidationError
                };
                userErrors.Errors.Add(newError);
            }
        }

        public async Task UserNameIsExistAsync(ApplicationUser userData, UserErrorsResponse userErrors, bool updating = false)
        {
            var user = await _userRepository.FindByNameAsync(userData.UserName);

            bool addCondition = updating ? user != null && userData.Id != user.Id : true;

            if (user != null && addCondition)
            {
                var newError = new UserErrorsItem
                {
                    UserField = UserField.UserName,
                    Message = Messages.userNameAlreadyError
                };
                userErrors.Errors.Add(newError);
            }
        }

        public async Task EmailIsExistAsync(ApplicationUser userData, UserErrorsResponse userErrors, bool updating = false)
        {
            var user = await _userRepository.FindByEmailAsync(userData.Email);

            bool addCondition = updating ? user != null && userData.Id != user.Id : true;

            if (user != null && addCondition)
            {
                var newError = new UserErrorsItem
                {
                    UserField = UserField.Email,
                    Message = Messages.emailAlreadyUsedError
                };
                userErrors.Errors.Add(newError);
            }
        }

        public void EmaiByPattern(ApplicationUser userData, UserErrorsResponse userErrors)
        {
            if (!new Regex(Patterns.emailPatern).IsMatch(userData.Email))
            {
                var newError = new UserErrorsItem
                {
                    UserField = UserField.Email,
                    Message = Messages.emailValidationError
                };
                userErrors.Errors.Add(newError);
            }
        }

        public void PasswordByPattern(string password, UserErrorsResponse userErrors)
        {
            if (!new Regex(Patterns.passwordPattern).IsMatch(password))
            {
                var newError = new UserErrorsItem
                {
                    UserField = UserField.Password,
                    Message = Messages.passwordCharactersError
                };
                userErrors.Errors.Add(newError);
            }
        }

        public async Task UserIsExistOrDisapbledAsync(string email, UserErrorsResponse userErrors)
        {
            ApplicationUser user = await _userRepository.FindByEmailAsync(email);

            if (user == null || user.IsDisabled)
            {
                var newError = new UserErrorsItem
                {
                    UserField = UserField.Email,
                    Message = Messages.userDisabledEdrror
                };
                userErrors.Errors.Add(newError);
            }
        }

        public async Task CheckEmail(string email, UserErrorsResponse userErrors)
        {
            ApplicationUser user = await _userRepository.FindByEmailAsync(email);
            if (user.Email.ToLower() != email.ToLower())
            {
                var newError = new UserErrorsItem
                {
                    UserField = UserField.Email,
                    Message = Messages.incorrectEmailError
                };
                userErrors.Errors.Add(newError);
            }
        }

        public void PasswordVerification(ApplicationUser userData, string password, UserErrorsResponse userErrors)
        {
            PasswordVerificationResult passwordVerificationResult = _userRepository.VerificationHashPassword(userData, userData.PasswordHash, password);

            if (!passwordVerificationResult.Equals(PasswordVerificationResult.Success))
            {
                var newError = new UserErrorsItem
                {
                    UserField = UserField.Password,
                    Message = Messages.passwordError
                };
                userErrors.Errors.Add(newError);
            }
        }

        public void HadlingIdentityResult(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                throw new ProjectException(StatusCodes.Status400BadRequest, result.Errors.ToString());
            }
        }

        public void HandlingUserSerchResult(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ProjectException(StatusCodes.Status404NotFound, Messages.userNotExistError);
            }
        }
    }
}
