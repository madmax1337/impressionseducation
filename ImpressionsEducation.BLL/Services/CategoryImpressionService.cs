﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.BLL.Models.CategoryImpressions.Request;
using ImpressionsEducation.BLL.Models.CategoryImpressions.Response;
using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.Common.Exceptions;
using ImpressionsEducation.Common.Models.Category;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;

namespace ImpressionsEducation.BLL.Services
{
    public class CategoryImpressionService : ICategoryImpressionService
    {
        private readonly ICategoryImpressionRepository _categoryImpressionRepository;
        private readonly IMapper _mapper;

        public CategoryImpressionService(ICategoryImpressionRepository categoryImpressionRepository, IMapper mapper)
        {
            _categoryImpressionRepository = categoryImpressionRepository;
            _mapper = mapper;
        }

        public async Task<BaseResponse> Create(CreateCategoryImpressionsRequest requestModel)
        {
            CategoryImpression categoryImpression = _mapper.Map<CreateCategoryImpressionsRequest, CategoryImpression>(requestModel);
            var queryResult = await _categoryImpressionRepository.Insert(categoryImpression);
            if (queryResult == null)
            {
                throw new ProjectException("Create category error");
            }
            return new BaseResponse { IsSuccessful = true };
        }

        public async Task<BaseResponse> GetAllCategory(FilterCategoryImpressionsRequest requestModel)
        {
            AllCategoryImpressionsResponse responseModel = new AllCategoryImpressionsResponse() { IsSuccessful = true }; 
            var filterModel = _mapper.Map<FilterCategoryImpressionsRequest, CategoryFilterModel>(requestModel);
            var queryResult = await _categoryImpressionRepository.GetAllCategory(filterModel);

            if (!queryResult.Items.Any())
            {
                return responseModel;
            }

            responseModel.Items = _mapper.Map<IEnumerable<CategoryImpression>, List<AllCategoryImpressionsItem>>(queryResult.Items);
            responseModel.Total = queryResult.Total;

            return responseModel;
        }

        public async Task<BaseResponse> Update(UpdateCategoryImpressionsRequest requestModel)
        {
            var queryResult = await _categoryImpressionRepository.FindByIdAsync(requestModel.CategoryId);
            queryResult.IsRemoved = requestModel.IsRemoved;
            await _categoryImpressionRepository.Update(queryResult);
            return new BaseResponse { IsSuccessful = true };
        }
    }
}
