﻿using AutoMapper;
using ImpressionsEducation.BLL.Models;
using ImpressionsEducation.BLL.Models.User.Request;
using ImpressionsEducation.BLL.Models.User.Response;
using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.BLL.Services.SubServices;
using ImpressionsEducation.Common.Models.User;
using ImpressionsEducation.Common.Security;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ImpressionsEducation.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly UserValidationService _userValidationService;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, UserValidationService userValidationService, IMapper mapper)
        {
            _userRepository = userRepository;
            _userValidationService = userValidationService;
            _mapper = mapper;
        }

        public async Task<BaseResponse> GetAllAsync(AllUserRequest requestModel)
        {
            UserFilterModel filterModel = _mapper.Map<AllUserRequest, UserFilterModel>(requestModel);
            var queryResult = await _userRepository.GetAllAsync(filterModel);
            if (!queryResult.Items.Any())
            {
                return new AllUserResponse();
            }
            AllUserResponse responseModel = new AllUserResponse() { IsSuccessful = true };
            responseModel.Items = _mapper.Map<IEnumerable<ApplicationUser>, List<AllUserItem>>(queryResult.Items);
            responseModel.Total = queryResult.Total;
            return responseModel;
        }

        public async Task<BaseResponse> GetUserProfile(ClaimsPrincipal claims)
        {
            var user = await _userRepository.FindByIdAsync(claims.FindFirst(ProjectClaimsTypes.Identifier).Value);
            _userValidationService.HandlingUserSerchResult(user);
            UserProfileResponse responseModel = _mapper.Map<ApplicationUser, UserProfileResponse>(user);
            return responseModel;
        }

        public async Task<BaseResponse> SetStatusAsync(DisableEnableUserRequest requestModel)
        {
            ApplicationUser user = await _userRepository.FindByIdAsync(requestModel.Id);
            _userValidationService.HandlingUserSerchResult(user);

            user.IsDisabled = requestModel.IsDisable;

            var queryResult = await _userRepository.UpdateAsync(user);
            _userValidationService.HadlingIdentityResult(queryResult);

            BaseResponse responseModel = new BaseResponse() { IsSuccessful = true };

            return responseModel;
        }

        public async Task<BaseResponse> UpdateAsync(UpdateUserRequest requestModel)
        {
            BaseResponse responseModel = new BaseResponse() { IsSuccessful = true };
            var user = await CompletitionUpdateModel(requestModel);
            var userValidationView = await _userValidationService.UpdateValidationAsync(user);

            if (userValidationView.IsSuccessful == false)
            {
                userValidationView.UserId = user.Id;
                return userValidationView;
            }

            var queryResult = await _userRepository.UpdateAsync(user);
            _userValidationService.HadlingIdentityResult(queryResult);

            if (!string.IsNullOrWhiteSpace(requestModel.Password) && !string.IsNullOrWhiteSpace(requestModel.NewPassword))
            {
                queryResult = await _userRepository.ChangePasswordAsync(user, requestModel.Password, requestModel.NewPassword);
                responseModel.Message = !queryResult.Succeeded ? "Curent password not valid" : string.Empty;
                responseModel.IsSuccessful = queryResult.Succeeded;
            }

            return responseModel;
        }

        private async Task<ApplicationUser> CompletitionUpdateModel(UpdateUserRequest requestModel)
        {
            var user = await _userRepository.FindByIdAsync(requestModel.UserId);
            _userValidationService.HandlingUserSerchResult(user);

            if (requestModel.IsRemove)
            {
                user.IsRemoved = requestModel.IsRemove;
                return user;
            }

            user.FirstName = requestModel.FirstName;
            user.LastName = requestModel.LastName;
            user.UserName = requestModel.UserName;
            user.Email = requestModel.Email;

            if (!string.IsNullOrWhiteSpace(requestModel.Image))
            {
                user.Image = requestModel.Image;
            }

            return user;
        }
    }
}
