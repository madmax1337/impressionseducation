﻿using AutoMapper;
using ImpressionsEducation.BLL.Mapper;
using ImpressionsEducation.BLL.Services;
using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.BLL.Services.SubServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ImpressionsEducation.BLL
{
    public class Configuration
    {
        public static void Add(IServiceCollection services, IConfiguration configuration)
        {
            DAL.Configuration.Add(services, configuration.GetConnectionString("DefaultConnection"));
            ConfigurationAutomapper(services);
            AddDependencies(services);
        }

        private static void ConfigurationAutomapper(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AccountMapper());
                mc.AddProfile(new UserMapper());
                mc.AddProfile(new OrderMapper());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        private static void AddDependencies(IServiceCollection services)
        {
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ICategoryImpressionService, CategoryImpressionService>();
            services.AddTransient<IComentaryService, ComentaryService>();
            services.AddTransient<IFeedBackWebsiteService, FeedBackWebsiteService>();
            services.AddTransient<IImpessionsService, ImpessionsService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<ILogExceptionService, LogExeptionService>();
            services.AddTransient<UserValidationService>();
            services.AddTransient<EmailService>();
        }
    }
}
