﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;
using TableAttribute = System.ComponentModel.DataAnnotations.Schema.TableAttribute;

namespace ImpressionsEducation.DAL.Entities
{
    [Table("Impressions")]
    public class Impression: BaseEntity
    {
        [Column("NameImpressions")]
        public string NameImpressions { get; set; }

        [Column("Description")]
        public string Description { get; set; }

        [Column("Price")]
        public int Price { get; set; }

        [Column("CountOfPeople")]
        public int? CountOfPeople { get; set; }

        [Column("TimeSpending")]
        public int? TimeSpending { get; set; }

        [ForeignKey("CategoryImpression")]
        [Column("CategoryImpressionId")]
        public string CategoryImpressionId { get; set; }

        [Computed]
        public CategoryImpression CategoryImpression { get; set; }

        [Column("Image")]
        public string Image { get; set; }

    }
}
