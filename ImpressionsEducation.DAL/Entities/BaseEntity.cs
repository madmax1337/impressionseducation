﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImpressionsEducation.DAL.Entities
{
    public class BaseEntity
    {
        [Column("Id")]
        [Key]
        public string Id { get; set; }

        [Column("CreationDate")]
        public DateTime CreationDate { get; set; }

        [Column("IsRemove")]
        public bool IsRemoved { get; set; }

        public BaseEntity()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.UtcNow;
        }
    }
}
