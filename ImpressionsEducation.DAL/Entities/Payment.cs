﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ImpressionsEducation.DAL.Entities
{
    [Table("Payments")]
    public class Payment: BaseEntity
    {
        [Column("TransactionId")]
        public string TransactionId { get; set; }
    }
}
