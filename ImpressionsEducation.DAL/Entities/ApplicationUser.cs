﻿using Microsoft.AspNetCore.Identity;
using System;

namespace ImpressionsEducation.DAL.Entities
{
    public class ApplicationUser: IdentityUser
    {
        public DateTime CreationDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsRemoved { get; set; }
        public string Image { get; set; }
        public int Role { get; set; }

        public ApplicationUser()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.UtcNow;
        }
    }
}
