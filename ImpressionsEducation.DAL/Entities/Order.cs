﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TableAttribute = System.ComponentModel.DataAnnotations.Schema.TableAttribute;

namespace ImpressionsEducation.DAL.Entities
{
    [Table("Orders")]
    public class Order: BaseEntity
    {
        [Column("Description")]
        public string Description { get; set; }

        [ForeignKey("User")]
        [Column("UserId")]
        public string UserId { get; set; }

        [Computed]
        public ApplicationUser User { get; set; }

        [ForeignKey("Payment")]
        [Column("PaymentId")]
        public string PaymentId { get; set; }

        [Computed]
        public Payment Payment { get; set; }

        [Column("OrderAmout")]
        public decimal OrderAmout { get; set; }

        [Column("OrderStatus")]
        public int OrderStatus { get; set; }

        [Computed]
        public ICollection<OrderItem> OrderItems { get; set; }

        public Order()
        {
            OrderItems = new List<OrderItem>();
        }
    }
}
