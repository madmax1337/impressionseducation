﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;
using TableAttribute = System.ComponentModel.DataAnnotations.Schema.TableAttribute;

namespace ImpressionsEducation.DAL.Entities
{
    [Table("Comentaries")]
    public class Comentary: BaseEntity
    {
        [ForeignKey("ApplicationUser")]
        [Column("UserId")]
        public string UserId { get; set; }

        [Computed]
        public ApplicationUser ApplicationUser { get; set; }

        [ForeignKey("Impression")]
        [Column("ImpressionId")]
        public string ImpressionId { get; set; }

        [Computed]
        public Impression Impression { get; set; }

        [Column("Description")]
        public string Description { get; set; }
    }
}
