﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ImpressionsEducation.DAL.Entities
{
    [Table("CategoryImpressions")]
    public class CategoryImpression: BaseEntity
    {
        [Column("NameCategory")]
        public string NameCategory { get; set; }
    }
}
