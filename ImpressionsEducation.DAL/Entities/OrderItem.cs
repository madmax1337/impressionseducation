﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;
using TableAttribute = System.ComponentModel.DataAnnotations.Schema.TableAttribute;

namespace ImpressionsEducation.DAL.Entities
{
    [Table("OrderItems")]
    public class OrderItem: BaseEntity
    {
        [Column("Amount")]
        public decimal Amount { get; set; }

        [ForeignKey("Impression")]
        [Column("ImpressionsId")]
        public string ImpressionsId { get; set; }

        [Computed]
        public Impression Impression { get; set; }

        [Column("Count")]
        public int Count { get; set; }

        [ForeignKey("Order")]
        [Column("OrderId")]
        public string OrderId { get; set; }

        [Computed]
        public Order Order { get; set; }
    }
}
