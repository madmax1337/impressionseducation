﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ImpressionsEducation.DAL.Entities
{
    public class FeedBackWebsite: BaseEntity
    {
        [Computed]
        public ApplicationUser User { get; set; }

        [ForeignKey("ApplicationUser")]
        [Column("UserId")]
        public string UserId { get; set; }

        [Column("Description")]
        public string Description { get; set; }
    }
}
