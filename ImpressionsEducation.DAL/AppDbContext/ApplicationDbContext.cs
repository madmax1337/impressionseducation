﻿using ImpressionsEducation.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ImpressionsEducation.DAL.AppDbContext
{
    public class ApplicationDbContext: IdentityDbContext<ApplicationUser, UserRole, string>
    {
        public DbSet<CategoryImpression> CategoryImpressions { get; set; }
        public DbSet<Comentary> Comentaries { get; set; }
        public DbSet<Impression> Impressions { get; set; }
        public DbSet<LogException> LogExceptions { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Payment> Payments { get; set; }

        public DbSet<FeedBackWebsite> FeedBackWebsites { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        { 
        
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ApplicationUser>().Property(u => u.LastName).IsConcurrencyToken();
            builder.Entity<ApplicationUser>().Property(u => u.Email).IsConcurrencyToken();
            base.OnModelCreating(builder);
        }
    }
}
