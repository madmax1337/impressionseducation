﻿using ImpressionsEducation.DAL.AppDbContext;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Initializations;
using ImpressionsEducation.DAL.Repository;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace ImpressionsEducation.DAL
{
    public class Configuration
    {
        public static void Add(IServiceCollection services, string connectionString)
        {
            AddDbContext(services, connectionString);
            AddItentity(services);
            AddDependecies(services, connectionString);
            Inizialization(services);
        }

        private static void AddDbContext(IServiceCollection services, string connectionString)
        {
            var migrationsAssembly = typeof(Configuration).GetTypeInfo().Assembly.GetName().Name;
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(connectionString, x => x.MigrationsAssembly(migrationsAssembly));
            }, ServiceLifetime.Transient); 
        }

        private static void AddItentity(IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, UserRole>(
                    options =>
                    {
                        options.Password.RequireDigit = false;
                        options.Password.RequiredLength = 8;
                        options.Password.RequireNonAlphanumeric = false;
                        options.Password.RequireUppercase = false;
                        options.Password.RequireLowercase = false;
                    })
                    .AddEntityFrameworkStores<ApplicationDbContext>()
                    .AddDefaultTokenProviders();
        }

        private static void Inizialization(IServiceCollection services)
        {
            ServiceProvider serviceProvider = services.BuildServiceProvider();

            using (var context = serviceProvider.GetRequiredService<ApplicationDbContext>())
            {
                if ((context.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
                {
                    DbInitialization.SeedDataAsync(services);
                }
            }
        }

        public static void AddDependecies(IServiceCollection services, string connectionString)
        {
            services.AddScoped<IDbConnection, SqlConnection>(c => new SqlConnection(connectionString));
            services.AddTransient(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            services.AddTransient<IFeedBackWebsiteRepository, FeedBackWebsiteRepository>();
            services.AddTransient<ICategoryImpressionRepository, CategoryImpressionRepository>();
            services.AddTransient<IComentaryRepository, ComentaryRepository>();
            services.AddTransient<IImpressionRepository, ImpressionRepository>();
            services.AddTransient<ILogExceptionRepository, LogExceptionRepository>();
            services.AddTransient<IOrderItemRepository, OrderItemRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
        }
    }
}
