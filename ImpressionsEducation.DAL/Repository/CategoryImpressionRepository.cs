﻿using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EnumsNET;
using ImpressionsEducation.Common.Models;
using ImpressionsEducation.Common.Models.Category;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ImpressionsEducation.DAL.Repository
{
    public class CategoryImpressionRepository : BaseRepository<CategoryImpression>, ICategoryImpressionRepository
    {
        public CategoryImpressionRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<PaginationModel<CategoryImpression>> GetAllCategory(CategoryFilterModel filterModel)
        {
            PaginationModel<CategoryImpression> queryResult = new PaginationModel<CategoryImpression>();

            var condition = new StringBuilder(string.IsNullOrWhiteSpace(filterModel.NameCategory) ? $@"AND NameCategory LIKE @SerchString+'%'" : string.Empty);

            var querySort = new StringBuilder($@"ORDER BY {(filterModel.CategoryColumnName).AsString(EnumFormat.Description)} {(filterModel.SortOrder).AsString(EnumFormat.Description)}");

            var queryPagination = new StringBuilder($@"OFFSET (@Page - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY");

            var queryData = new StringBuilder($@"SELECT *
                                             FROM CategoryImpressions
                                             WHERE IsRemoved = 0");

            var queryCount = new StringBuilder($@"; SELECT COUNT(*)
                                             FROM CategoryImpressions
                                             WHERE IsRemoved = 0");

            string sql = $@"{queryData.ToString()}
                            {condition.ToString()}
                            {querySort.ToString()}
                            {queryPagination.ToString()}";

            string sqlCount = $@"{queryCount.ToString()}
                                 {condition.ToString()}";

            using (IDbConnection db = CreateConnection())
            {
                var result = await db.QueryMultipleAsync(sql + sqlCount, new
                {
                    Page = filterModel.Page,
                    PageSize = filterModel.PageSize,
                    SearchString = filterModel.NameCategory
                });

                queryResult.Total = result.Read<int>().FirstOrDefault();
                queryResult.Items = result.Read<CategoryImpression>();
            }

            return queryResult;
        }
    }
}
