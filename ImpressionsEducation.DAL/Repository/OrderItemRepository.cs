﻿using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ImpressionsEducation.DAL.Repository
{
    public class OrderItemRepository: BaseRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(IConfiguration configuration) : base(configuration) { }
    }
}
