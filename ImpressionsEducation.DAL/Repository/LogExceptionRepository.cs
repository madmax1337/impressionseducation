﻿using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ImpressionsEducation.DAL.Repository
{
    public class LogExceptionRepository: BaseRepository<LogException>, ILogExceptionRepository
    {
        public LogExceptionRepository(IConfiguration configuration) : base(configuration) { }
    }
}
