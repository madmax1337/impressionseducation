﻿using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ImpressionsEducation.DAL.Repository
{
    public class ImpressionRepository: BaseRepository<Impression>, IImpressionRepository
    {
        public ImpressionRepository(IConfiguration configuration) : base(configuration) { }
    }
}
