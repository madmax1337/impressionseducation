﻿using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ImpressionsEducation.DAL.Repository
{
    public class FeedBackWebsiteRepository: BaseRepository<FeedBackWebsite>, IFeedBackWebsiteRepository
    {
        public FeedBackWebsiteRepository(IConfiguration configuration) : base(configuration) { }
    }
}
