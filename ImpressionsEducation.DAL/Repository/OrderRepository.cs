﻿using System.Text;
using System.Threading.Tasks;
using EnumsNET;
using ImpressionsEducation.Common.Enums;
using ImpressionsEducation.Common.Models;
using ImpressionsEducation.Common.Models.Order;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Linq;

namespace ImpressionsEducation.DAL.Repository
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<PaginationModel<DataOrderItemModel>> GetOrderAsync(OrderFilterModel orderFilterModel)
        {
            var paginationModel = new PaginationModel<DataOrderItemModel>();

            var query = new StringBuilder(@"FROM ImpressionEducation.[dbo].OrderItems AS oi
                                            INNER JOIN ImpressionEducation.[dbo].Impressions AS im ON im.Id = oi.ImpressionsId
                                            INNER JOIN (SELECT *
                                            FROM ImpressionEducation.[dbo].Orders AS o
                                            WHERE o.IsRemove = 0");

            var queryData = new StringBuilder(@"SELECT o.Id as OrderId,
                                                o.CreationDate as DateOrder,
                                                OrderAmount,
                                                OrderStatus,
                                                anu.Id
                                                UserName, 
                                                Email,
                                                oi.Id,
                                                oi.Count as OrderCount,
                                                im.Id as ImpressionsId,
                                                NameImpressions");

            var queryCount = new StringBuilder(@"; SELECT COUNT (DISTINCT o.Id)");

            var querySort = new StringBuilder($@"ORDER BY {(orderFilterModel.OrderColumnName).AsString(EnumFormat.Description)} {(orderFilterModel.SortOrder).AsString(EnumFormat.Description)}");

            query.Append(string.IsNullOrWhiteSpace(orderFilterModel.UserId) ? $"AND o.UserId = {orderFilterModel.UserId}" : string.Empty);
            query.Append(!orderFilterModel.StatusOrder.Equals(StatusOrder.All) ? $"AND o.OrderStatus = {(int)orderFilterModel.StatusOrder}" : string.Empty);

            queryData.Append(query.ToString() + $@"ORDER BY o.Id
                                                 OFFSET @Page * (@PageSize - 1) FETCH NEXT @PageSize ROWS ONLY) AS o ON oi.OrderId = o.Id
                                                 INNER JOIN ImpressionEducation.[dbo].AspNetUsers AS anu ON o.UserId = anu.Id" + querySort.ToString());

            queryCount.Append(query.ToString() + $@")AS o ON oi.OrderId = o.Id
                                                    INNER JOIN ImpressionEducation.[dbo].AspNetUsers AS anu ON o.UserId = anu.Id");

            using (var connection = CreateConnection())
            {
                var result = await connection.QueryMultipleAsync(queryData.ToString() + queryCount.ToString(), new { Page = orderFilterModel.Page, PageSize = orderFilterModel.PageSize });

                var orders = result.Read<OrderItem, Impression, Order, ApplicationUser, OrderItem>(
                    (orderItem, impressions, order, user) =>
                    {
                        order.User = user;
                        orderItem.Order = order;
                        orderItem.Impression = impressions;
                        return orderItem;
                    },
                    splitOn: "Id, Id, Id, Id"
                    );

                paginationModel.Total = result.Read<int>().FirstOrDefault();
                paginationModel.Items = orders.GroupBy(x => x.Order.Id).Select(group => new DataOrderItemModel
                {
                    OrderId = group.Select(element => element.Id).FirstOrDefault(),
                    DateOrder = group.Select(element => element.CreationDate).FirstOrDefault(),
                    OrderAmount = group.Select(element => element.Amount).FirstOrDefault(),
                    OrderStatus = group.Select(element => element.Order.OrderStatus).FirstOrDefault(),
                    UserName = group.Select(element => element.Order.User.UserName).FirstOrDefault(),
                    UserEmail = group.Select(element => element.Order.User.Email).FirstOrDefault(),
                    OrderItems = group. Select(element => new OrderItemData
                    {
                        Count = element.Count,
                        NameImpressions = element.Impression.NameImpressions
                    }).ToList()
                });
            }

            return paginationModel;
        }
    }
}
