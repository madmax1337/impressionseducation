﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ImpressionsEducation.DAL.Repository
{
    public class ComentaryRepository : BaseRepository<Comentary>, IComentaryRepository
    {
        public ComentaryRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<IEnumerable<Comentary>> GetComentaryImpressions(string impressionsId)
        {

            var query = $@"SELECT *
                            FROM Impressions AS im
                            INNER JOIN Comentary AS com ON im.Id = com.ImmpressionsId
                            INNER JOIN AspNetUsers AS anu ON com.UserId = anu.Id
                            WHERE com.ImpressionsId = @impressionsId";

            using (IDbConnection db = CreateConnection())
            {
                var result = await db.QueryAsync<Comentary, Impression, ApplicationUser, Comentary>(query, (comentary, impressions, user) =>
                 {
                     comentary.ApplicationUser = user;
                     comentary.Impression = impressions;
                     return comentary;
                 },
                impressionsId,
                splitOn: "Id, Id, Id"
                );
                return result;
            }
        }
    }
}
