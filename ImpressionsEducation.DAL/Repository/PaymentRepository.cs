﻿using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ImpressionsEducation.DAL.Repository
{
    public class PaymentRepository: BaseRepository<Payment>, IPaymentRepository
    {
        public PaymentRepository(IConfiguration configuration) : base(configuration) { }
    }
}
