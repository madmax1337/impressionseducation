﻿using ImpressionsEducation.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImpressionsEducation.DAL.Repository.Interfaces
{
    public interface IComentaryRepository: IBaseRepository<Comentary>
    {
        Task<IEnumerable<Comentary>> GetComentaryImpressions(string impressionsId);  
    }
}
