﻿using ImpressionsEducation.Common.Models;
using ImpressionsEducation.Common.Models.Category;
using ImpressionsEducation.DAL.Entities;
using System.Threading.Tasks;

namespace ImpressionsEducation.DAL.Repository.Interfaces
{
    public interface ICategoryImpressionRepository: IBaseRepository<CategoryImpression>
    {
        Task<PaginationModel<CategoryImpression>> GetAllCategory(CategoryFilterModel filterModel);
    }
}
