﻿using ImpressionsEducation.Common.Models;
using ImpressionsEducation.Common.Models.Order;
using ImpressionsEducation.DAL.Entities;
using System.Threading.Tasks;

namespace ImpressionsEducation.DAL.Repository.Interfaces
{
    public interface IOrderRepository: IBaseRepository<Order>
    {
        Task<PaginationModel<DataOrderItemModel>> GetOrderAsync(OrderFilterModel orderFilterModel);
    }
}
