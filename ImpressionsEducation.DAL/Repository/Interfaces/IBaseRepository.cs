﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImpressionsEducation.DAL.Repository.Interfaces
{
    public interface IBaseRepository<Entity>
    {
        Task<IEnumerable<Entity>> GetAll();
        Task<Entity> Insert(Entity entity, bool withPrimary = true);
        Task Insert(IEnumerable<Entity> entity, bool withPrimary = false);
        Task Update(Entity entity);
        Task Update(IEnumerable<Entity> entities);
        Task Delete(string id);
        Task<Entity> FindByIdAsync(string id);
    }
}
