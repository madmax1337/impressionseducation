﻿using ImpressionsEducation.Common.Models;
using ImpressionsEducation.Common.Models.User;
using ImpressionsEducation.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ImpressionsEducation.DAL.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<DateTimeOffset?> CheckLockoutStatusAsync(ApplicationUser user);
        Task<IdentityResult> CreateAsync(ApplicationUser item);
        Task<IdentityResult> UpdateAsync(ApplicationUser item);
        Task<IdentityResult> RemoveAsync(ApplicationUser item);
        Task<IdentityResult> SetLockOutAsync(string email, bool lockout);
        Task<ApplicationUser> FindByIdAsync(string id);
        Task<IdentityResult> AddToRoleAsync(ApplicationUser item, string role);
        Task<IEnumerable<string>> GetRoleAsync(ApplicationUser item);
        Task<SignInResult> LoginAsync(string email, string password);
        Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string newPassword);
        Task LogOutAsync();
        Task<IdentityResult> ConfirmEmailAsync(ApplicationUser item, string code);
        Task<string> GenerateEmailTokenAsync(ApplicationUser item);
        Task<bool> IsEmailConfirmedAsync(ApplicationUser item);
        Task<ApplicationUser> FindByNameAsync(string name);
        Task<ApplicationUser> FindByEmailAsync(string email);
        Task<ApplicationUser> GetUserAsync(ClaimsPrincipal principal);
        Task<PaginationModel<ApplicationUser>> GetAllAsync(UserFilterModel userFilterModel);
        Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword);
        string HashPassword(ApplicationUser user, string password);
        PasswordVerificationResult VerificationHashPassword(ApplicationUser user, string hashedPassword, string providedPassword);
    }
}
