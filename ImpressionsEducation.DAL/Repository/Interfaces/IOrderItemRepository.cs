﻿using ImpressionsEducation.DAL.Entities;

namespace ImpressionsEducation.DAL.Repository.Interfaces
{
    public interface IOrderItemRepository: IBaseRepository<OrderItem>
    {
    }
}
