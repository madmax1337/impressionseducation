﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using ImpressionsEducation.Common.Enums;
using ImpressionsEducation.Common.Models;
using ImpressionsEducation.Common.Models.User;
using ImpressionsEducation.DAL.AppDbContext;
using ImpressionsEducation.DAL.Entities;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ImpressionsEducation.DAL.Repository
{
    public class UserRepository : IUserRepository
    {

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserRepository(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<IdentityResult> AddToRoleAsync(ApplicationUser item, string role)
        {
            return await _userManager.AddToRoleAsync(item, role);
        }

        public async Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword)
        {
            return await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);
        }

        public async Task<DateTimeOffset?> CheckLockoutStatusAsync(ApplicationUser user)
        {
            return await _userManager.GetLockoutEndDateAsync(user);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(ApplicationUser item, string code)
        {
            return await _userManager.ConfirmEmailAsync(item, code);
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser item)
        {
            return await _userManager.CreateAsync(item);
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<ApplicationUser> FindByIdAsync(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<ApplicationUser> FindByNameAsync(string name)
        {
            return await _userManager.FindByNameAsync(name);
        }

        public async Task<string> GenerateEmailTokenAsync(ApplicationUser item)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(item);
        }

        public async Task<IEnumerable<string>> GetRoleAsync(ApplicationUser item)
        {
            return await _userManager.GetRolesAsync(item);
        }

        public async Task<ApplicationUser> GetUserAsync(ClaimsPrincipal principal)
        {
            return await _userManager.GetUserAsync(principal);
        }

        public string HashPassword(ApplicationUser user, string password)
        {
            string passwordHash = _userManager.PasswordHasher.HashPassword(user, password);
            return passwordHash;
        }

        public async Task<bool> IsEmailConfirmedAsync(ApplicationUser item)
        {
            return await _userManager.IsEmailConfirmedAsync(item);
        }

        public async Task<SignInResult> LoginAsync(string email, string password)
        {
            var user = await _userManager.FindByEmailAsync(email);
            return await _signInManager.CheckPasswordSignInAsync(user, password, lockoutOnFailure: false);
        }

        public async Task LogOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<IdentityResult> RemoveAsync(ApplicationUser item)
        {
            return await _userManager.DeleteAsync(item);
        }

        public async Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string newPassword)
        {
            var token = await GeneratePasswordResetTokenAsync(user);
            return await _userManager.ResetPasswordAsync(user, token, newPassword);
        }

        public async Task<IdentityResult> SetLockOutAsync(string email, bool lockout)
        {
            var user = await FindByEmailAsync(email);
            return await _userManager.SetLockoutEndDateAsync(user, DateTime.Now.AddYears(100));
        }

        public async Task<IdentityResult> UpdateAsync(ApplicationUser item)
        {
            return await _userManager.UpdateAsync(item);
        }

        public PasswordVerificationResult VerificationHashPassword(ApplicationUser user, string hashedPassword, string providedPassword)
        {
            PasswordVerificationResult passwordVerificationResult = _userManager.PasswordHasher.VerifyHashedPassword(user, hashedPassword, providedPassword);
            return passwordVerificationResult;
        }

        private async Task<string> GeneratePasswordResetTokenAsync(ApplicationUser item)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(item);
        }

        public async Task<PaginationModel<ApplicationUser>> GetAllAsync(UserFilterModel userFilterModel)
        {
            var query = _userManager.Users.Where(x => x.IsRemoved == false && x.Role != (int)Role.Admin);
            var model = new PaginationModel<ApplicationUser>();

            if (!string.IsNullOrWhiteSpace(userFilterModel.SearchString))
            {
                query = query.Where(x => x.UserName.StartsWith(userFilterModel.SearchString) || x.Email.StartsWith(userFilterModel.SearchString));
            }

            if (userFilterModel.LockoutStatus.Equals(LockoutStatus.Active))
            {
                query = query.Where(x => !x.IsDisabled);
            }

            if (userFilterModel.LockoutStatus.Equals(LockoutStatus.Blocked))
            {
                query = query.Where(x => x.IsDisabled);
            }

            query = Sort(query, userFilterModel.SortOrder, userFilterModel.SortFieldUser);

            model.Total = query.Count();
            model.Items = await query.Skip((userFilterModel.Page - 1) * userFilterModel.PageSize)
                                     .Take(userFilterModel.PageSize).ToListAsync();

            return model;
        }

        private IQueryable<ApplicationUser> Sort(IQueryable<ApplicationUser> users, SortOrder sortOrder, UserField sortFieldUser)
        {
            Expression<Func<ApplicationUser, object>> expression = null;

            if (UserField.UserName == sortFieldUser)
            {
                expression = user => user.UserName;
                users = UsersOrderBy(sortOrder, users, expression);
            }

            if (UserField.Email == sortFieldUser)
            {
                expression = user => user.Email;
                users = UsersOrderBy(sortOrder, users, expression);
            }

            if (UserField.FirstName == sortFieldUser)
            {
                expression = user => user.FirstName;
                users = UsersOrderBy(sortOrder, users, expression);
            }

            if (UserField.LastName == sortFieldUser)
            {
                expression = user => user.LastName;
                users = UsersOrderBy(sortOrder, users, expression);
            }

            return users;
        }

        private IQueryable<ApplicationUser> UsersOrderBy(SortOrder sortOrder, IQueryable<ApplicationUser> users, Expression<Func<ApplicationUser, object>> expression)
        {
            if (SortOrder.Ascending == sortOrder)
            {
                users = users.OrderBy(expression);
            }

            if (SortOrder.Descending == sortOrder)
            {
                users = users.OrderByDescending(expression);
            }

            return users;
        }
    }
}
