﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Dapper;
using ImpressionsEducation.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ImpressionsEducation.DAL.Repository
{
    public class BaseRepository<Entity> : IBaseRepository<Entity>
    {
        protected string passedTableName;
        protected readonly string _connectionString;

        public BaseRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            passedTableName = typeof(Entity).GetCustomAttribute<TableAttribute>().Name;
        }

        public Task Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IEnumerable<Entity>> GetAll()
        {
            string sql = $@"SELECT * FROM {passedTableName}";

            using (IDbConnection db = CreateConnection())
            {
                return await db.QueryAsync<Entity>(sql);
            }
        }

        public async Task<Entity> FindByIdAsync(string id)
        {
            string sql = $@"SELECT * FROM {passedTableName} WHERE Id = {id}";
            using (IDbConnection db = CreateConnection())
            {
                return (await db.QueryAsync<Entity>(sql)).ToList().FirstOrDefault();
            }
        }

        public async Task<Entity> Insert(Entity entity, bool withPrimary = true)
        {
            IEnumerable<string> columnsAttributeName = GetColumnsAttributeName(withPrimary);

            IEnumerable<string> columnsName = GetColumnsName(withPrimary);

            string stringOfColumns = string.Join(", ", columnsAttributeName.Select(e => e));

            string stringOfParameters = string.Join(", ", columnsAttributeName.Select((e, index) => "@" + columnsName.ElementAt(index)));

            string sql = $"INSERT INTO {passedTableName} ({stringOfColumns}) VALUES ({stringOfParameters})";

            using (IDbConnection db = CreateConnection())
            {
                await db.ExecuteAsync(sql, entity);
                return entity;
            }
        }

        public async Task Insert(IEnumerable<Entity> entity, bool withPrimary = false)
        {
            IEnumerable<string> columnsAttributeName = GetColumnsAttributeName(withPrimary);

            IEnumerable<string> columnsName = GetColumnsName(withPrimary);

            string stringOfColumns = string.Join(", ", columnsAttributeName.Select(e => e));

            string stringOfParameters = string.Join(", ", columnsName.Select((e, index) => "@" + columnsName.ElementAt(index)));

            string sql = $"INSERT INTO {passedTableName} ({stringOfColumns}) VALUES ({stringOfParameters})";

            using (IDbConnection db = CreateConnection())
            {
                using (IDbTransaction tran = db.BeginTransaction())
                {
                    try
                    {
                        await db.ExecuteAsync(sql, entity, transaction: tran);
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw;
                    }
                }
            }
        }

        public async Task Update(Entity entity)
        {
            IEnumerable<string> columnsAttributeName = GetColumnsAttributeName(false);

            IEnumerable<string> columnsName = GetColumnsName(false);

            string stringOfColumns = string.Join(", ", columnsAttributeName.Select((e, index) => $"{e} = @{columnsName.ElementAt(index)}"));

            string attributeNamePrimary = GetPrimaryKeyName(true);

            string namePrimary = GetPrimaryKeyName(false);

            string sql = $"UPDATE {passedTableName} SET {stringOfColumns} WHERE {attributeNamePrimary} = @{namePrimary}";

            using (IDbConnection db = CreateConnection())
            {
                await db.ExecuteAsync(sql, entity);
            }
        }

        public async Task Update(IEnumerable<Entity> entities)
        {
            IEnumerable<string> columnsAttributeName = GetColumnsAttributeName(false);

            IEnumerable<string> columnsName = GetColumnsName(false);

            string stringOfColumns = string.Join(", ", columnsAttributeName.Select((e, index) => $"{e} = @{columnsName.ElementAt(index)}"));

            string attributeNamePrimary = GetPrimaryKeyName(true);

            string namePrimary = GetPrimaryKeyName(false);

            string condition = $"WHERE {attributeNamePrimary} = @{namePrimary}";

            string sql = $"UPDATE {passedTableName} SET {stringOfColumns} {condition}";

            using (IDbConnection db = CreateConnection())
            {
                using (var transaction = db.BeginTransaction())
                {
                    try
                    {
                        var affectedRows = await db.ExecuteAsync(sql, entities, transaction: transaction);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        protected IEnumerable<string> GetColumnsName(bool withPrimaryKey)
        {
            IEnumerable<string> result = Enumerable.Empty<string>();

            IEnumerable<PropertyInfo> properties = typeof(Entity).GetProperties().Where(e => !Attribute.IsDefined(e, typeof(NotMappedAttribute)));

            if (withPrimaryKey)
            {
                result = properties.Select(e => e.Name);
            }

            if (!withPrimaryKey)
            {
                result = properties.Where(e => !Attribute.IsDefined(e, typeof(KeyAttribute))).Select(e => e.Name);
            }

            return result;
        }

        protected IEnumerable<string> GetColumnsAttributeName(bool withPrimaryKey)
        {
            IEnumerable<string> result = Enumerable.Empty<string>();

            IEnumerable<PropertyInfo> properties = typeof(Entity).GetProperties().Where(e => !Attribute.IsDefined(e, typeof(NotMappedAttribute)));

            if (withPrimaryKey)
            {
                result = properties.Select(e => e.GetCustomAttribute<ColumnAttribute>().Name);
            }

            if (!withPrimaryKey)
            {
                result = properties.Where(e => !Attribute.IsDefined(e, typeof(KeyAttribute))).Select(e => e.GetCustomAttribute<ColumnAttribute>().Name);
            }

            return result;
        }

        protected string GetPrimaryKeyName(bool attributeName)
        {
            string result = string.Empty;

            IEnumerable<PropertyInfo> propertyInfos = typeof(Entity).GetProperties().Where(e => Attribute.IsDefined(e, typeof(KeyAttribute)));

            if (attributeName)
            {
                result = propertyInfos.Select(e => e.GetCustomAttribute<ColumnAttribute>().Name).FirstOrDefault();
            }

            if (!attributeName)
            {
                result = propertyInfos.Select(e => e.Name).FirstOrDefault();
            }

            return result;
        }

        protected IDbConnection CreateConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();

            return connection;
        }
    }
}
