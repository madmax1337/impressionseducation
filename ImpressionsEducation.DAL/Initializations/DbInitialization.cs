﻿using ImpressionsEducation.Common.Enums;
using ImpressionsEducation.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImpressionsEducation.DAL.Initializations
{
    public class DbInitialization
    {
        //private readonly UserManager<ApplicationUser> _userManager;
        //private readonly RoleManager<UserRole> _roleManager;
        //private const string _email = "maksim131998@gmail.com";
        //private const string _firstName = "Rudyka";
        //private const string _lastName = "Maksim";
        //private const string _password = "7f34jih434hA";

        public static void SeedDataAsync(IServiceCollection services)
        {
            IServiceProvider serviceProvider = services.BuildServiceProvider();
            UserManager<ApplicationUser> userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            RoleManager<UserRole> roleManager = serviceProvider.GetRequiredService<RoleManager<UserRole>>();
            SeedRolesAsync(roleManager).GetAwaiter();
            SeedSuperUserAsync(userManager).GetAwaiter();
        }

        private static async Task SeedRolesAsync(RoleManager<UserRole> roleManager)
        {
            IEnumerable<Role> allRoles = Enum.GetValues(typeof(Role)).Cast<Role>();

            foreach (Role role in allRoles)
            {
                if (!roleManager.Roles.Any(r => r.Name == role.ToString()))
                {
                    await roleManager.CreateAsync(new UserRole
                    {
                        Name = role.ToString(),
                        NormalizedName = role.ToString().ToUpper()
                    });
                }
            }
        }

        private static async Task SeedSuperUserAsync(UserManager<ApplicationUser> userManager)
        {
            if (!userManager.Users.Any(u => u.Email == "admin@gmail.com"))
            {
                ApplicationUser user = new ApplicationUser();

                user.UserName = "Admin";
                user.Email = "admin@gmail.com";
                user.FirstName = "Rudyka";
                user.LastName = "Maksim";
                user.Password = userManager.PasswordHasher.HashPassword(user, "Admin123!");
                user.PasswordHash = user.Password;
                user.EmailConfirmed = true;
                user.Role = (int)Role.Admin;

                await userManager.CreateAsync(user);

                string roleToCreate = Role.Admin.ToString();
                await userManager.AddToRoleAsync(user, roleToCreate);
            }
        }
    }
}
