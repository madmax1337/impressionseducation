﻿using ImpressionsEducation.Common.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace ImpressionsEducation.Api.Helpers
{
    public class AuthorizeHelper : IAsyncAuthorizationFilter
    {
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (context.HttpContext.Response.StatusCode == StatusCodes.Status419AuthenticationTimeout)
            {
                if (!context.HttpContext.Request.Path.ToString().Equals("/Account/RefreshToken"))
                {
                    throw new ProjectException(StatusCodes.Status419AuthenticationTimeout);
                }
            }
        }
    }

}
