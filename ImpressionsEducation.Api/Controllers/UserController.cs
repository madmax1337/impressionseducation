﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImpressionsEducation.Api.Helpers;
using ImpressionsEducation.BLL.Models.User.Request;
using ImpressionsEducation.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ImpressionsEducation.Api.Controllers
{
    [Route("[controller]/[action]")]
    [TypeFilter(typeof(AuthorizeHelper))]
    //[Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetUserProfile()
        {
            var responseModel = await _userService.GetUserProfile(User);
            return Ok(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody]UpdateUserRequest requestModel)
        {
            var responseMode = await _userService.UpdateAsync(requestModel);
            return Ok(responseMode);
        }

        [HttpPost]
        public async Task<IActionResult> ViewAll([FromBody]AllUserRequest requestModel)
        {
            var responseModel = await _userService.GetAllAsync(requestModel);
            return Ok(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> DisableEnabled([FromBody] DisableEnableUserRequest requestModel)
        {
            var responseModel = await _userService.SetStatusAsync(requestModel);
            return Ok(responseModel);
        }
    }
}