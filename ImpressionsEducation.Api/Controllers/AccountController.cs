﻿using ImpressionsEducation.Api.Helpers;
using ImpressionsEducation.BLL.Models.Account.Request;
using ImpressionsEducation.BLL.Models.Account.Response;
using ImpressionsEducation.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImpressionsEducation.Api.Controllers
{
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            ConfirmEmailResponse responseModel = await _accountService.ConfirmEmailAsync(userId, code) as ConfirmEmailResponse;
            var url = $"http://localhost:4200/auth/confirm-email";
            url += !string.IsNullOrWhiteSpace(responseModel.Message) ? $" errors= {responseModel.Message}" : string.Empty;
            return Redirect(url);
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordRequest requestModel)
        {
            var responseModel = await _accountService.ForgotPasswordAsync(requestModel);
            return Ok(responseModel);
        }

        [HttpGet]
        public async Task<IActionResult> LogOut()
        {
            await _accountService.LogOutAsync();
            HttpContext.Session.Clear();
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]SignInRequest requestModel)
        {
            var responseModel = await _accountService.SignInAsync(requestModel);
            return Ok(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> Registration([FromBody]SignUpRequest requestModel)
        {
            var responseModel = await _accountService.SignUpAsync(requestModel);
            return Ok(responseModel);
        }

        [HttpPost]
        [TypeFilter(typeof(AuthorizeHelper))]
        public async Task<IActionResult> RefreshToken([FromBody]RefreshTokenAuthenticationRequest requestModel)
        {
            var responseModel = await _accountService.RefreshToken(requestModel);
            return Ok(responseModel);
        }
    }
}