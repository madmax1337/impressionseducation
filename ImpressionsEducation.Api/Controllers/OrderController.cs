﻿using ImpressionsEducation.Api.Helpers;
using ImpressionsEducation.BLL.Models.Order.Request;
using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.Common.Security;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImpressionsEducation.Api.Controllers
{
    [Route("[controller]/[action]")]
    [TypeFilter(typeof(AuthorizeHelper))]
    public class OrderController : Controller
    {
        private readonly IOrderService _service;

        public OrderController(IOrderService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateOrderRequest requestModel)
        {
            var responseModel = await _service.CreateOrder(requestModel, User.FindFirst(ProjectClaimsTypes.Identifier).Value);
            return Ok(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody] UpdateOrderRequest requestModel)
        {
            var responseModel = await _service.UpdateOrder(requestModel);
            return Ok(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> GetOrders([FromBody]OrderFilterRequest requestModel)
        {
            var responseModel = await _service.GetAll(requestModel);
            return Ok(responseModel);
        }
    }
}