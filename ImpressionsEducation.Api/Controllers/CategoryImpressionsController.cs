﻿using ImpressionsEducation.Api.Helpers;
using ImpressionsEducation.BLL.Models.CategoryImpressions.Request;
using ImpressionsEducation.BLL.Services;
using ImpressionsEducation.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImpressionsEducation.Api.Controllers
{
    [Route("[controller]/[action]")]
    [TypeFilter(typeof(AuthorizeHelper))]
    public class CategoryImpressionsController : Controller
    {

        private readonly ICategoryImpressionService _service;

        public CategoryImpressionsController(ICategoryImpressionService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateCategoryImpressionsRequest requestModel)
        {
            var responseModel = await _service.Create(requestModel);
            return Ok(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody]UpdateCategoryImpressionsRequest requestModel)
        {
            var responseModel = await _service.Update(requestModel);
            return Ok(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> Delete([FromBody]UpdateCategoryImpressionsRequest requestModel)
        {
            var responseModel = await _service.Update(requestModel);
            return Ok(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> GetAllCategory([FromBody]FilterCategoryImpressionsRequest requestModel)
        {
            var responseModel = await _service.GetAllCategory(requestModel);
            return Ok(responseModel);
        }
    }
}