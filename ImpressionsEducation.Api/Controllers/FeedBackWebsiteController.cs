﻿using ImpressionsEducation.Api.Helpers;
using ImpressionsEducation.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ImpressionsEducation.Api.Controllers
{
    [Route("[controller]/[action]")]
    [TypeFilter(typeof(AuthorizeHelper))]
    public class FeedBackWebsiteController : Controller
    {
        private readonly IFeedBackWebsiteService _service;

        public FeedBackWebsiteController(IFeedBackWebsiteService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Create(string description)
        {
            var responseModel = await _service.Create(User, description);
            return Ok(responseModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetComentary()
        {
            var responseModel = await _service.GetCometary();
            return Ok(responseModel);
        }
    }
}