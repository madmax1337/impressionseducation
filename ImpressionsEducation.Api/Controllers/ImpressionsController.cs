﻿using ImpressionsEducation.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ImpressionsEducation.Api.Controllers
{
    [Route("[controller]/[action]")]
    public class ImpressionsController : Controller
    {
        private readonly IImpessionsService _service;

        public ImpressionsController(IImpessionsService service)
        {
            _service = service;
        }
    }
}