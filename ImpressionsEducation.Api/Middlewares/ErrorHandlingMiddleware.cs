﻿using ImpressionsEducation.BLL.Services.Interfaces;
using ImpressionsEducation.Common.Exceptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ImpressionsEducation.Api.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogExceptionService _logExceptionService;

        public ErrorHandlingMiddleware(RequestDelegate next, ILogExceptionService logExceptionService)
        {
            _next = next;
            _logExceptionService = logExceptionService;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        public Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            int code = (int)HttpStatusCode.InternalServerError;

            string message = exception.Message;

            if (exception is ProjectException)
            {
                code = (exception as ProjectException).StatusCode;
            }

            _logExceptionService.Create(exception, context.Request.Path);

            context.Response.ContentType = "application/json";

            context.Response.StatusCode = code;

            return context.Response.WriteAsync(message);
        }
    }
}
