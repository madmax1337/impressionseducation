﻿using ImpressionsEducation.Common.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ImpressionsEducation.Api.Extensions
{
    public class OptionExtension
    {
        public static void Add(IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();
            services.Configure<AuthTokenProviderOption>(configuration.GetSection(nameof(AuthTokenProviderOption)));
            services.Configure<EmailOption>(configuration.GetSection(nameof(EmailOption)));
        }
    }
}
