﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ImpressionsEducation.Api.Extensions
{
    public class CorsExtension
    {
        public static void Add(IServiceCollection services, IConfiguration configuration)
        {
            IConfigurationSection corsOptions = configuration.GetSection("Cors");
            var origins = corsOptions["Origins"];
            services.AddCors(options =>
            {
                options.AddPolicy("AllPolicy", b => b.AllowAnyHeader()
                                                          .AllowAnyMethod()
                                                          .AllowAnyOrigin()
                                                          .AllowCredentials());

                options.AddPolicy("OriginPolicy", b => b.WithOrigins(origins)
                                                        .AllowAnyHeader()
                                                        .AllowAnyMethod()
                                                        .AllowCredentials());
            });
        }
    }
}
