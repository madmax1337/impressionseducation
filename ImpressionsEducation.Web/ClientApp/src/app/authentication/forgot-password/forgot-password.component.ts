import { Component } from '@angular/core';
import { BaseResponse } from 'src/app/shared/models/base-response';
import { ForgotPasswordRequest } from 'src/app/shared/models/authentication';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { PatternConstants } from 'src/app/shared/constants/pattern-constants';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash"
import { SpinnerHelper } from 'src/app/shared/helpers/spinner.helper';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {

  validationModel: BaseResponse
  //forgotPasswordModel:ForgotPasswordRequest;
  forgotPasswordForm: FormGroup;
  isValidFormSubmitted: boolean;

  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private toastrService: ToastrService,
    private spinnerHelper: SpinnerHelper) {
    this.formGroup();
  }

  private formGroup(): void {
    this.forgotPasswordForm = this.formBuilder.group({
      email: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.EMAIL_PATTERN)])
    });
  }

  submit(): void {

    this.spinnerHelper.show();

    if (this.forgotPasswordForm.invalid) {
      this.spinnerHelper.hide();
      return;
    }

    let model: ForgotPasswordRequest = _.cloneDeep(this.forgotPasswordForm.value);

    this.authenticationService.forgotPassword(model).subscribe((data: BaseResponse) => {

      if (data.isSuccessful) {
        this.isValidFormSubmitted = true;
        this.spinnerHelper.hide();
        return
      }
      this.toastrService.error(data.message);
      this.spinnerHelper.hide();
    });
  }
}
