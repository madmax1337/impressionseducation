import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-confirm-password',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  hideConfirmEmail: boolean;
  error: string;
  firstName: string;
  lastName: string;

  constructor(private router: Router,
    private route: ActivatedRoute, ) { }

  ngOnInit() {
    this.getInformation();
  }

  private getInformation(): void {
    if (this.route.snapshot.queryParamMap.get('errors')) {
      this.error = this.route.snapshot.queryParamMap.get('errors');
      this.hideConfirmEmail = true;
    }

    if (this.route.snapshot.queryParamMap.get('firstName') && this.route.snapshot.queryParamMap.get('lastName')) {
      this.firstName = this.route.snapshot.queryParamMap.get('firstName');
      this.lastName = this.route.snapshot.queryParamMap.get('lastName');
    }
  }

  continue(): void {
    this.router.navigate(["/auth/login"]);
  }
}
