import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserErrorsResponse, SignInRequest, JwtAuthenticationResponse } from 'src/app/shared/models/authentication';
import { UserField } from 'src/app/shared/enums';
import { AuthHelper } from 'src/app/shared/helpers/auth.helper';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Router } from '@angular/router';
import { BaseResponse } from 'src/app/shared/models/base-response';
import { SpinnerHelper } from 'src/app/shared/helpers/spinner.helper';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public loginForm: FormGroup;
  responseErrors: UserErrorsResponse;
  userField = UserField;
  hide: boolean;

  constructor(private formBuilder: FormBuilder,
    private authHelper: AuthHelper,
    private accountService: AuthenticationService,
    private router: Router,
    private spinner: SpinnerHelper) {
    this.buildForm();
  }

  private buildForm() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    });
  }

  get formControls() { return this.loginForm.controls; }

  private completionOfLoginModel(): SignInRequest {

    let loginModel = new SignInRequest();

    loginModel.email = this.formControls.email.value;
    loginModel.password = this.formControls.password.value;

    return loginModel;
  }

  login() {
    this.spinner.show();

    if(this.loginForm.invalid){
      return;
    }

    let model: SignInRequest = this.completionOfLoginModel();

    this.accountService.login(model).subscribe((response: BaseResponse) => {

      if (!response.isSuccessful) {
        this.responseErrors = response as UserErrorsResponse;

        this.responseErrors.errors.forEach(data => {
          this.setErrorFormControl(data.userField, data.message);
        });

        this.spinner.hide();
        return;
      }

      this.authHelper.login(response as JwtAuthenticationResponse);
      this.authHelper.notifieSignInResult();
      this.router.navigate(['/managment']);
      this.spinner.hide();
    });
  }

  private setErrorFormControl(validationField?: UserField, messageError?: string): void {

    if (validationField === this.userField.Email) {
      return this.formControls.email.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userField.Password) {
      return this.formControls.password.setErrors({ used: true, message: messageError });
    }

    this.loginForm.setErrors(null)
  }
}
