import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { PatternConstants } from 'src/app/shared/constants/pattern-constants';
import { SpinnerHelper } from 'src/app/shared/helpers/spinner.helper';
import { SignUpRequest, UserErrorsResponse, UserErrorsItem } from 'src/app/shared/models/authentication';
import * as _ from "lodash"
import { UserField } from 'src/app/shared/enums';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {

  userValidationData: UserErrorsResponse
  hidePasword: boolean;
  hideConfirmPassword: boolean;
  signUpForm: FormGroup;
  isValidFormSubmitted: boolean;
  visibleSpiner: boolean;
  url: string;
  reader: FileReader;

  userFields = UserField;

  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private spinnerHelper: SpinnerHelper) {
    this.reader = new FileReader();

    this.formGroup();
  }

  readonly formControlsName = {
    firstName: 'firstName',
    lastName: 'lastName',
    userName: 'userName',
    email: 'email',
    password: 'password',
    passwordConfirm: 'passwordConfirm'
  }

  private formGroup(): void {
    this.signUpForm = this.formBuilder.group({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.NAME_PATTERN), Validators.minLength(2), Validators.maxLength(30)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.NAME_PATTERN), Validators.minLength(2), Validators.maxLength(50)]),
      userName: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      email: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.EMAIL_PATTERN)]),
      password: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.PASSWORD_PATTERN)]),
      passwordConfirm: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.PASSWORD_PATTERN)])
    });
  }

  get formControls() { return this.signUpForm.controls; }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      this.reader.readAsDataURL(event.target.files[0]);
      this.reader.onload = () => {
        this.url = this.reader.result.toString();
      };
    }
  }

  registration(): void {
    this.spinnerHelper.show();
    this.matchPasswords();

    if (this.signUpForm.invalid) {
      this.spinnerHelper.hide();
      return;
    }

    let model: SignUpRequest = _.cloneDeep(this.signUpForm.value);
    model.image = this.url;

    this.authenticationService.registration(model).subscribe((data: UserErrorsResponse) => {

      if (!data.isSuccessful) {
        this.userValidationData = data;
        this.userValidationData.errors.forEach((item: UserErrorsItem) => {
          this.setErrorFormControl(item.userField, item.message)
        });
        this.spinnerHelper.hide();
        return;
      }

      this.isValidFormSubmitted = true;
    });
  }

matchPasswords(): void{
  if(this.formControls.password.value !== this.formControls.passwordConfirm.value){
    this.formControls.passwordConfirm.setErrors({ used: true, message: "Passwords doesn't match" });
    this.formControls.password.setErrors({ used: true, message: "Passwords doesn't match" });
  }
  this.signUpForm.setErrors(null)
}

  checkErrorsFormControl(formControlName: string): ValidationErrors {
      return this.formControls[formControlName].errors
  }

  private setErrorFormControl(validationField?: UserField, messageError?: string): void {

    if (validationField === this.userFields.FirstName) {
      return this.formControls.firstName.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userFields.LastName) {
      return this.formControls.lastName.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userFields.UserName) {
      return this.formControls.userNames.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userFields.Email) {
      return this.formControls.email.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userFields.Password) {
      return this.formControls.password.setErrors({ used: true, message: messageError });
    }

    this.signUpForm.setErrors(null)
  }
}
