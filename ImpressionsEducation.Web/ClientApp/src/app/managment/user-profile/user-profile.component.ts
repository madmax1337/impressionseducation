import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { PatternConstants } from 'src/app/shared/constants/pattern-constants';
import { UserService } from 'src/app/core/services/user.service';
import { UserProfileResponse, UpdateUserRequest } from 'src/app/shared/models/user';
import * as _ from "lodash"
import { SpinnerHelper } from 'src/app/shared/helpers/spinner.helper';
import { ToastrService } from 'ngx-toastr';
import { UserErrorsItem, UserErrorsResponse } from 'src/app/shared/models/authentication';
import { UserField } from 'src/app/shared/enums';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent {

  hidePasword: boolean;
  hideNewPassword: boolean;
  userForm: FormGroup;
  isEdit: boolean;
  isReadOnly = true;
  image: string;
  isPhotoEmpty: boolean;
  reader = new FileReader();
  userValidationData: UserErrorsResponse;
  userFields = UserField;
  userId: string;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private userSpinnerHelper: SpinnerHelper,
    private toasterService: ToastrService) {
    this.userSpinnerHelper.show();
    this.formGroup();
    this.getUserProfile();
  }

  readonly formControlsName = {
    firstName: 'firstName',
    lastName: 'lastName',
    userName: 'userName',
    email: 'email',
    password: 'password',
    newPassword: 'newPassword'
  }

  private getUserProfile() {
    this.userService.getUserProfile().subscribe((data: UserProfileResponse) => {
      if (data) {
        this.userForm.patchValue(data);
        this.image = data.image;
        this.isPhotoEmpty = this.image ? true : false;
        this.userId = data.id;
        this.userSpinnerHelper.hide();
        return;
      }
      this.userSpinnerHelper.hide()
      return;
    });
  }

  private formGroup(): void {
    this.userForm = this.formBuilder.group({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.NAME_PATTERN), Validators.minLength(2), Validators.maxLength(30)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.NAME_PATTERN), Validators.minLength(2), Validators.maxLength(50)]),
      userName: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      email: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.EMAIL_PATTERN)]),
      password: new FormControl(null, [Validators.pattern(PatternConstants.PASSWORD_PATTERN)]),
      newPassword: new FormControl(null, [Validators.pattern(PatternConstants.PASSWORD_PATTERN)])
    });
  }

  get formControls() { return this.userForm.controls; }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      this.reader.readAsDataURL(event.target.files[0]);
      this.reader.onload = () => {
        this.image = this.reader.result.toString();
      };
    }

    this.isPhotoEmpty = this.image ? true : false;
  }

  edit(): void {
    this.isEdit = true;
    this.isReadOnly = false;
  }

  cancel(): void {
    this.isEdit = false;
    this.isReadOnly = true;
    this.isPhotoEmpty = !this.image ? false : true;
  }

  save(): void {
    this.userSpinnerHelper.show();

    if (this.userForm.invalid) {
      this.userSpinnerHelper.hide();
      return;
    }

    let model: UpdateUserRequest = _.cloneDeep(this.userForm.value);
    model.userId = this.userId;

    this.userService.update(model).subscribe((data: UserErrorsResponse) => {

      if (!data.isSuccessful && data.message === null) {
        this.userValidationData = data;
        this.userValidationData.errors.forEach((item: UserErrorsItem) => {
          this.setErrorFormControl(item.userField, item.message)
        });
        this.userSpinnerHelper.hide();
        return;
      }

      if (!data.isSuccessful && data.message !== null) {
        this.toasterService.error(data.message);
        this.userSpinnerHelper.hide();
        return;
      }

      this.toasterService.success("Your profile updated");
      this.isEdit = false;
      this.isReadOnly = true;
      this.isPhotoEmpty = !this.image ? false : true;
      this.getUserProfile();
      this.userSpinnerHelper.hide();
    })
  }

  private setErrorFormControl(validationField?: UserField, messageError?: string): void {

    if (validationField === this.userFields.FirstName) {
      return this.formControls.firstName.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userFields.LastName) {
      return this.formControls.lastName.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userFields.UserName) {
      return this.formControls.userNames.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userFields.Email) {
      return this.formControls.email.setErrors({ used: true, message: messageError });
    }

    if (validationField === this.userFields.Password) {
      return this.formControls.password.setErrors({ used: true, message: messageError });
    }

    this.userForm.setErrors(null)
  }

  checkErrorsFormControl(formControlName: string): ValidationErrors {
    return this.formControls[formControlName].errors;
  }
}
