import { Component, OnInit, InjectionToken } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../core/services/authentication.service';
import { AuthHelper } from '../shared/helpers/auth.helper';
import { LocalStorageHelper } from '../shared/helpers/local-storage.helper';
import { ScrollStrategy, Overlay } from '@angular/cdk/overlay';

@Component({
  selector: 'app-managment',
  templateUrl: './managment.component.html',
  styleUrls: ['./managment.component.scss']
})
export class ManagmentComponent implements OnInit {

  isLoggedIn: boolean;
  subscriptionLogin: Subscription;
  subscriptionCart: Subscription;
  role: string;
  userName: string;
  route: string;
  cartItemsLength: number;
  isEmptyCart: boolean;
  image: string;
  isPhotoEmpty: boolean;

  constructor(private authHelper: AuthHelper) { }

  ngOnInit() {
    this.getSubscription();
  }


  getSubscription(): void {
    this.subscriptionLogin = this.authHelper.getLoggedIn().subscribe((data: boolean) => {
      this.isLoggedIn = data;
      this.UserInforMation();
      this.cartItemsLength = 0;
    });

    // this.subscriptionCart = this.cartService.cartSubject.subscribe((data: CartModelItem[]) => {

    //   if (data.length !== 0) {
    //     this.cartItemsLength = data.length;
    //   }
    //   if (data.length === 0) {
    //     this.cartItemsLength = this.localStorageService.Cart ? JSON.parse(this.localStorageService.Cart).length : 0;
    //   }

    // });
  }

  UserInforMation(): void {
    this.userName = this.authHelper.getFullName();
    this.role = this.authHelper.getRoleFromToken();
  }

  logOut(): void {
    // const dialogRef = this.dialog.open(DialogLogoutComponent, { data: this.userName });
    // dialogRef.afterClosed().subscribe((result: string) => {
    //   if (result != null) {
    this.authHelper.logout();
    //   }
    // });
  }

  ngOnDestroy(): void {
    this.subscriptionLogin.unsubscribe();
    // this.subscriptionCart.unsubscribe();
  }
}
