import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AllUserItem } from 'src/app/shared/models/user';
import { PatternConstants } from 'src/app/shared/constants/pattern-constants';
import * as _ from "lodash"

@Component({
  selector: 'app-dialog-edit-user',
  templateUrl: './dialog-edit-user.component.html',
  styleUrls: ['./dialog-edit-user.component.scss']
})
export class DialogEditUserComponent {

  userForm: FormGroup;

  readonly formControlsName = {
    firstName: 'firstName',
    lastName: 'lastName',
    userName: 'userName',
    email: 'email'
  }

  constructor(private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DialogEditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public userData: AllUserItem) {
    this.formGroup();
    this.userForm.patchValue(userData);
  }

  private formGroup(): void {
    this.userForm = this.formBuilder.group({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.NAME_PATTERN), Validators.minLength(2), Validators.maxLength(30)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.NAME_PATTERN), Validators.minLength(2), Validators.maxLength(50)]),
      userName: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      email: new FormControl(null, [Validators.required, Validators.pattern(PatternConstants.EMAIL_PATTERN)])
    });
  }

  get formControls() { return this.userForm.controls; }

  checkErrorsFormControl(formControlName: string): ValidationErrors {
    return this.formControls[formControlName].errors
  }

  closeWindow(): void {
    this.dialogRef.close();
  }

  saveData(): void {
    this.userData = _.cloneDeep(this.userForm.value);
    this.dialogRef.close(this.userData);
  }
}
