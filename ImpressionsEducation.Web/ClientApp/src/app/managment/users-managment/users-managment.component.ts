import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { AllUserItem, AllUserRequest, DisabledEnabledUserRequest, AllUserResponse, UpdateUserRequest } from 'src/app/shared/models/user';
import { UserService } from 'src/app/core/services/user.service';
import { BaseResponse } from 'src/app/shared/models/base-response';
import { ToastrService } from 'ngx-toastr';
import { SpinnerHelper } from 'src/app/shared/helpers/spinner.helper';
import { Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { SortOrder, UserField, LockoutStatus } from 'src/app/shared/enums';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { DialogEditUserComponent } from './dialog-edit-user/dialog-edit-user.component';
import * as _ from "lodash"
import { ValidationErrors } from '@angular/forms';
import { UserErrorsResponse, UserErrorsItem } from 'src/app/shared/models/authentication';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-users-managment',
  templateUrl: './users-managment.component.html',
  styleUrls: ['./users-managment.component.scss']
})
export class UsersManagmentComponent implements OnInit {

  @ViewChild('paginatorUser', { static: true }) paginatorUser: MatPaginator;

  dataSource: MatTableDataSource<AllUserItem>;
  filter: AllUserRequest;

  displayedColumns: string[] = ['firstName', 'lastName', 'userName', 'email', 'status', 'crud'];

  pageSizeOptions: number[];
  pageTotal: number;

  sortOrder: SortOrder;
  userField: UserField;
  toSortUser: boolean;

  lockoutStatusOption: Array<CheckBoxItem>;

  constructor(private userService: UserService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private spinnerUser: SpinnerHelper) {
    this.pageSizeOptions = [5, 10, 25];
    this.dataSource = new MatTableDataSource<AllUserItem>();
    this.filter = new AllUserRequest();
    this.lockoutStatusOption = new Array<CheckBoxItem>();
    this.lockoutStatusOption.push(new CheckBoxItem(LockoutStatus.Active, LockoutStatus[LockoutStatus.Active], true));
    this.lockoutStatusOption.push(new CheckBoxItem(LockoutStatus.Blocked, LockoutStatus[LockoutStatus.Blocked], true));

  }

  ngOnInit() {
    this.spinnerUser.show();
    this.toFirstPageUser();
    this.getUsers();
  }

  toFirstPageUser(): void {
    this.paginatorUser.pageIndex = 0;
    this.paginatorUser.pageSize = 2;
  }

  getUsers(): void {
    this.spinnerUser.show();
    this.onToggle();
    this.completitionOfSerchUser()
    this.userService.viewAll(this.filter).subscribe((data: AllUserResponse) => {

      if (data.isSuccessful && data.items.length) {
        this.dataSource.data = data.items;
        this.pageTotal = data.total;
        this.spinnerUser.hide();
        return
      }

      if (!data.items.length) {
        this.dataSource = new MatTableDataSource<AllUserItem>();
      }
    })
  }

  private completitionOfSerchUser(): void {
    this.filter.page = this.paginatorUser.pageIndex + 1;
    this.filter.pageSize = this.paginatorUser.pageSize;

    this.filter.sortFieldUser = this.userField;
    this.filter.sortOrder = this.sortOrder;
    this.filter.toSort = this.toSortUser;
  }

  sortUser(sort: Sort): void {
    if (sort === null) {
      return;
    }
    const asc: string = "asc";

    this.sortOrder = sort.direction === asc ? SortOrder.Ascending : SortOrder.Descending;
    this.userField = UserField[sort.active];
    this.toSortUser = sort.direction === null ? false : true;
    this.getUsers();
  }

  setLockout(item: AllUserItem, checked: boolean): void {
    this.spinnerUser.show();

    let model: DisabledEnabledUserRequest = new DisabledEnabledUserRequest();
    model.isDisabled = checked;
    model.id = item.id

    this.userService.disabledEnable(model).subscribe((data: BaseResponse) => {

      if (!data.isSuccessful) {
        this.toastr.error(data.message);
        this.spinnerUser.hide();
        return;
      }
      this.toastr.success(!checked ? 'User disabled' : 'User enabled');
      this.spinnerUser.hide();
    });
  }

  onChecked(eventEmitter: MatCheckboxChange, id: number): void {
    if (!this.lockoutStatusOption.some(x => x.checked === true)) {
      eventEmitter.source.toggle();
      this.lockoutStatusOption[id].checked = true;
    }
  }

  onToggle(): void {
    const checkedOptions = this.lockoutStatusOption.filter(x => x.checked);
    this.filter.lockoutStatus = checkedOptions[0].value;
    if (checkedOptions.length === 2) {
      this.filter.lockoutStatus = LockoutStatus.All;
    }
  }

  editUser(element: AllUserItem): void {
    const dialogRef = this.dialog.open(DialogEditUserComponent, {
      data: element,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result: AllUserItem) => {
      if (result != null) {

        this.spinnerUser.show();

        let model: UpdateUserRequest = _.cloneDeep(result);
        model.userId = element.id
        this.userService.update(model).subscribe((data: UserErrorsResponse) => {
          if (!data.isSuccessful) {

            data.errors.forEach((item: UserErrorsItem) => {
              this.toastr.error(item.message);
            });

            this.spinnerUser.hide();
            return;
          }
          this.toastr.success(`User ${model.userName + ' ' + model.firstName} has been updated`);
          this.getUsers()
        });
      }
    });
  }
}

export class CheckBoxItem {

  value: number;
  label: string;
  checked: boolean;

  constructor(value: any, label: any, checked?: boolean) {
    this.value = value;
    this.label = label;
    this.checked = checked ? checked : false;
  }

}
