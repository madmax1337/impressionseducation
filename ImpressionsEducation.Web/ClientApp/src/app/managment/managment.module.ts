import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagmentComponent } from './managment.component';
import { MamagmentRoutingModule } from './mamagment-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field'
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UsersManagmentComponent } from './users-managment/users-managment.component';
import { MatTableModule } from '@angular/material/table';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DialogEditUserComponent } from './users-managment/dialog-edit-user/dialog-edit-user.component';
import { MatDialogModule } from '@angular/material/dialog';
import { OrderManagmentComponent } from './order-managment/order-managment.component';

@NgModule({
  imports: [
    CommonModule,
    MamagmentRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatBadgeModule,
    MatMenuModule,
    MatTableModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    FormsModule,
    MatDialogModule,

  ],
  declarations: [
    ManagmentComponent,
    UserProfileComponent,
    UsersManagmentComponent,
    DialogEditUserComponent,
    OrderManagmentComponent
  ],
  entryComponents: [
    DialogEditUserComponent
  ]
})
export class ManagmentModule { }
