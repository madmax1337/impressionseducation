import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagmentComponent } from './managment.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UsersManagmentComponent } from './users-managment/users-managment.component';
import { OrderManagmentComponent } from './order-managment/order-managment.component';

const routes: Routes = [
  {
    path: '', component: ManagmentComponent,
    children: [
      { path: 'user-profile', component: UserProfileComponent },
      { path: 'user-managment', component: UsersManagmentComponent },
      { path: 'order-managment', component: OrderManagmentComponent }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MamagmentRoutingModule { }
