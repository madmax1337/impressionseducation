import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError, EMPTY } from 'rxjs';
import { catchError, switchMap, filter, take, finalize } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

//Helpers
import { AuthHelper } from '../../shared/helpers/auth.helper';
import { SpinnerHelper } from '../../shared/helpers/spinner.helper';

//Services
import { AuthenticationService } from '../services/authentication.service';

//Models
import { JwtAuthenticationResponse, RefreshTokenAuthenticationRequest } from 'src/app/shared/models/authentication';

@Injectable()
export class Interceptor implements HttpInterceptor {
  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private authHelper: AuthHelper,
    private spinner: SpinnerHelper,
    private authenticationService: AuthenticationService,
    private toastrService: ToastrService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authHelper.getAccessToken()) {
      request = this.addToken(request, this.authHelper.getAccessToken());
    }

    return next.handle(request).pipe(catchError(error => {
      this.spinner.hide();
      if (error instanceof HttpErrorResponse && error.status === 419) {
        return this.handle419Error(request, next);
      }
      console.log(error);
      if (error instanceof HttpErrorResponse && error.status === 401) {
        this.authHelper.logout();
        return throwError(error);
      }
      if (error instanceof HttpErrorResponse && error.status === 403) {
        this.authHelper.logout();
      }
      else {
        this.toastrService.error(error.error);
        return throwError(error);
      }
    }));

  }

  private handle419Error(request: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshing) {
      this.isRefreshing = true;

      this.refreshTokenSubject.next(null);

      return this.refresh().pipe(
        switchMap((user: JwtAuthenticationResponse) => {
          console.clear();
          if (user) {
            this.refreshTokenSubject.next(user.accessToken);
            this.authHelper.login(user);
            return next.handle(this.addToken(request, user.accessToken));
          }
          this.authHelper.logout();
        }),
        finalize(() => {
          this.isRefreshing = false;
        }));
    }

    return this.refreshTokenSubject.pipe(
      filter(token => token != null),
      take(1),
      switchMap(jwt => {
        return next.handle(this.addToken(request, jwt));
      }), catchError(err => {
        return throwError(err);
      }));
  }

  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        'Authorization': `Bearer ${token}`
      }
    });
  }

  private refresh(): Observable<JwtAuthenticationResponse> {
    let refreshToken = this.authHelper.getRefreshToken();
    if (!refreshToken) {
      this.authHelper.logout();
      return EMPTY;
    }
    let model: RefreshTokenAuthenticationRequest = new RefreshTokenAuthenticationRequest();
    model.refreshToken = refreshToken
    return this.authenticationService.refreshToken(model) as Observable<JwtAuthenticationResponse>;
  }
}
