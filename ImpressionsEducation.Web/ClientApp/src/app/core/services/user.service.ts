import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { BaseResponse } from 'src/app/shared/models/base-response';
import { UpdateUserRequest, AllUserRequest, DisabledEnabledUserRequest } from 'src/app/shared/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly apiControllerName: string = 'User';
  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl + this.apiControllerName;
  }

  getUserProfile(): Observable<BaseResponse> {
    return this.http.get<BaseResponse>(this.apiUrl + '/GetUserProfile');
  }

  update(model: UpdateUserRequest): Observable<BaseResponse> {
    return this.http.post<BaseResponse>(this.apiUrl + '/Update', model);
  }

  viewAll(model: AllUserRequest): Observable<BaseResponse> {
    return this.http.post<BaseResponse>(this.apiUrl + '/ViewAll', model);
  }

  disabledEnable(model: DisabledEnabledUserRequest){
    return this.http.post<BaseResponse>(this.apiUrl + '/DisableEnabled', model);
  }
}
