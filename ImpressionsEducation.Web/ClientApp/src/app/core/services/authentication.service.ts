import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SignInRequest, ForgotPasswordRequest, SignUpRequest, RefreshTokenAuthenticationRequest } from 'src/app/shared/models/authentication';
import { Observable } from 'rxjs';
import { BaseResponse } from 'src/app/shared/models/base-response';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly apiControllerName: string = 'Account';
  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl + this.apiControllerName;
  }

  login(requestModel: SignInRequest): Observable<BaseResponse> {
    return this.http.post<BaseResponse>(this.apiUrl + '/Login', requestModel);
  }

  forgotPassword(requestModel: ForgotPasswordRequest): Observable<BaseResponse> {
    return this.http.post<BaseResponse>(this.apiUrl + '/ForgotPassword', requestModel);
  }

  registration(requestModel: SignUpRequest): Observable<BaseResponse> {
    return this.http.post<BaseResponse>(this.apiUrl + '/Registration', requestModel);
  }

  refreshToken(requestModel: RefreshTokenAuthenticationRequest): Observable<BaseResponse> {
    return this.http.post<BaseResponse>(this.apiUrl + '/RefreshToken', requestModel);
  }
}
