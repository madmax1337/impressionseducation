import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ManagmentComponent } from './managment/managment.component';


const routes: Routes = [
  {path: "", redirectTo: "managment", pathMatch: "full"},
  {path: 'auth', component: AppComponent, loadChildren: () => import('src/app/authentication/authentication.module').then(m => m.AuthenticationModule)},
  {path: 'managment', component: AppComponent, loadChildren: ()=>import('src/app/managment/managment.module').then(m=>m.ManagmentModule)},
  {path: "**", redirectTo: "managment"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
