import { Injectable } from '@angular/core';

@Injectable()

export class PatternConstants {
  static readonly NAME_PATTERN: string = "^[A-Za-z`'-]+$"
  static readonly PASSWORD_PATTERN: string = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!*&^%$£\"~#])[A-Za-z\\d!*&^%$£\"~#]{8,}$";
  static readonly EMAIL_PATTERN: string = "^[A-Za-z0-9]+((\.[A-Za-z0-9]+)*|(-[A-Za-z0-9]+)*|(_[A-Za-z0-9]+)*)@[A-Za-z0-9]+((-[A-Za-z0-9]+)*|(_[A-Za-z0-9]+)*)*\.[A-Za-z]{2,}$"
}
