export class BaseResponse {
  isSuccessful: boolean;
  message: string;
}
