import { LockoutStatus, SortOrder, UserField } from 'src/app/shared/enums';

export class AllUserRequest {
  page: number;
  pageSize: number;
  searchString: string;
  deleted: boolean;
  toSort: boolean;
  lockoutStatus: LockoutStatus;
  sortOrder: SortOrder;
  sortFieldUser: UserField;
}
