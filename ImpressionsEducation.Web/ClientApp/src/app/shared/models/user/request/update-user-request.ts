export class UpdateUserRequest {
  userId: string;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
  newPassword: string;
  image: string;
  isRemove: string;
}
