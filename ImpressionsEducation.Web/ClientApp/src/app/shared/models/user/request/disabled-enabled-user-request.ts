export class DisabledEnabledUserRequest {
  id: string;
  isDisabled: boolean;
}
