import { BaseResponse } from '../../base-response';

export class UserProfileResponse extends BaseResponse {
  id: string;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  image: string;
}
