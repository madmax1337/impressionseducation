import { BaseResponse } from '../../base-response';

export class AllUserResponse extends BaseResponse {
  items: Array<AllUserItem>;
  total: number;

  constructor(){
    super()
    this.items = new Array<AllUserItem>();
  }
}

export class AllUserItem {
  id: string;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  isDisabled: boolean;
}
