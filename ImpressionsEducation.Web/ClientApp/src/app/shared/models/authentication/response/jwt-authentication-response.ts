import { BaseResponse } from '../../base-response';

export class JwtAuthenticationResponse extends BaseResponse {
  userName: string;
  fullName: string;
  accessToken: string;
  refreshToken: string;
}
