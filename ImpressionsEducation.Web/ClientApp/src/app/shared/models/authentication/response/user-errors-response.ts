import { BaseResponse } from '../../base-response';
import { UserField } from 'src/app/shared/enums';

export class UserErrorsResponse extends BaseResponse {
  userId: string;
  errors: Array<UserErrorsItem>;
  constructor() {
    super();
    this.errors = new Array<UserErrorsItem>();
  }
}

export class UserErrorsItem {
  message: string;
  userField: UserField;
}
