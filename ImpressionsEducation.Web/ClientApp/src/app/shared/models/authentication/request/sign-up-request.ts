export class SignUpRequest {
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
  image: string;
}
