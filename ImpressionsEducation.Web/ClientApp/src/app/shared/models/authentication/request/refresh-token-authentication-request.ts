export class RefreshTokenAuthenticationRequest {
  refreshToken: string;
}
