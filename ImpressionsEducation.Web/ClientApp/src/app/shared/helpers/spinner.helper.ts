import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class SpinnerHelper {

constructor(private spinner: NgxSpinnerService) { }

show(): void {
  this.spinner.show(undefined, {
    bdColor: 'rgba(0,0,0, 0.6)',
    size: "medium",
    color: "#fff",
    type: "ball-spin-clockwise",
    fullScreen: true
  })
}

hide(): void {
  this.spinner.hide()
}

}
