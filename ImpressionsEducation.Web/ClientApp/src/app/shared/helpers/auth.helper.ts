import { Injectable } from '@angular/core';
import { JwtAuthenticationResponse } from '../models/authentication';
import { BehaviorSubject, Observable } from 'rxjs';
import { LocalStorageHelper } from './local-storage.helper';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthHelper {

  private readonly _userDataKey: string = 'USER_KEY';
  private _currentUserData: JwtAuthenticationResponse;

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.isAuthenticated());

  constructor(private localStorageHelper: LocalStorageHelper,
              private router: Router) {
    this._currentUserData = JSON.parse(localStorageHelper.getItem(this._userDataKey)) as JwtAuthenticationResponse;
  }

  login(data: JwtAuthenticationResponse): void {
    this._currentUserData = data;
    this.localStorageHelper.setItem(this._userDataKey, JSON.stringify(data));
  }

  logout(): void {
    this._currentUserData = null;

    localStorage.removeItem(this._userDataKey);

    this.notifieSignInResult();

    this.router.navigate(['managment']);

    //window.location.reload();
  }

  getLoggedIn(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  notifieSignInResult(): void {
    this.loggedIn.next(this.isAuthenticated());
  }

  getAccessToken(): string {
    if (this._currentUserData) {
      return this._currentUserData.accessToken;
    }
    return undefined;
  }

  getRefreshToken(): string {
    if (this._currentUserData) {
      return this._currentUserData.refreshToken;
    }
    return undefined;
  }

  isAuthenticated(): boolean {
    let data = JSON.parse(localStorage.getItem(this._userDataKey)) as JwtAuthenticationResponse;
    return !!data;
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (error) {
      return null;
    }
  }

  getUserIdFromToken(): string {
    const token: string = this.getAccessToken();
    if (token) {
      return this.getDecodedAccessToken(token).id;
    }
    return null;
  }

  getUserNameFromToken(): string {

    const userName: string = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";

    const token: string = this.getAccessToken();

    try {
      var tokenData = jwt_decode(token);
      return tokenData[userName];
    }
    catch (error) {
      return null;
    }
  }

  getRoleFromToken(): string {

    const role: string = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";

    const token: string = this.getAccessToken();

    try {
      var tokenData = jwt_decode(token);
      return tokenData[role];
    }
    catch (error) {
      return null;
    }
  }

  getFullName(): string {
    return this._currentUserData.fullName;
  }
}
