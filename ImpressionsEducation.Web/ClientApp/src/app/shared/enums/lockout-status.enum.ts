export enum LockoutStatus {
  Active = 0,
  Blocked = 1,
  All = 2
}
