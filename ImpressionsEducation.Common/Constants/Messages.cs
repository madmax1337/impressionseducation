﻿namespace ImpressionsEducation.Common.Constants
{
    public static class Messages
    {
        public const string refreshTokenExpired = "RefreshToken expired";
        public const string userExistError = "User does not exist.";
        public const string userNotFound = "User not found";
        public const string incorrectEmailError = "The Email is incorrect. Try again";
        public const string userDisabledEdrror = "User disabled or does not exist";
        public const string emailValidationError = "Email is incorect. Try again";
        public const string passwordError = "The Password is incorrect. Try again";
        public const string emailAlreadyUsedError = "This Email is already being used";
        public const string userNameAlreadyError = "This UserName is already being used";
        public const string firstNameValidationError = "Incorect First Name. Please enter between 2 and 30 characters (A-z`-)";
        public const string lastNameValidationError = "Incorect First Name. Please enter between 2 and 50 characters (A-z`-)";
        public const string passwordCharactersError = "Incorerct Password. Please enter 8 characters (A-z 0-9 *&^%$£\"~#)";
        public const string userNotExistError = "User is not exist";
        public const string detailNameAlreadyExist = "Detail name already exist";
        public const string errorValidationSellingDates = "Error validation selling dates";
        public const string emailSendError = "Sending letter in your email error";
        public const string confirmEmailError = "Error confirm account";
    }
}
