﻿namespace ImpressionsEducation.Common.Constants
{
    public static class Patterns
    {
        public const string passwordPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!*&^%$£\"~#])[A-Za-z\\d!*&^%$£\"~#]{8,}$";
        public const string firstNamePattern = "^([A-Za-z'`-]+){2,30}$";
        public const string lastNamePattern = "^([A-Za-z`'-]+){2,50}$";
        public const string emailPatern = @"^[A-Za-z0-9]+((\.[A-Za-z0-9]+)*|(-[A-Za-z0-9]+)*|(_[A-Za-z0-9]+)*)@[A-Za-z0-9]+((-[A-Za-z0-9]+)*|(_[A-Za-z0-9]+)*)*\.[A-Za-z]{2,}$";
    }
}
