﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionsEducation.Common.Constants
{
    public static class Email
    {
        public const string EmailConfirm = "Confirm your account";
        public const string ForgotPassword = "Forgot Password";
        public const string Name = "Administration";
    }
}
