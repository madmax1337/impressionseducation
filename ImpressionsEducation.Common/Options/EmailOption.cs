﻿namespace ImpressionsEducation.Common.Options
{
    public class EmailOption
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Smtp { get; set; }
        public int Port { get; set; }
    }
}
