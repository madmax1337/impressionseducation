﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionsEducation.Common.Options
{
    public class AuthTokenProviderOption
    {
        public string JwtKey { get; set; }
        public string JwtIssuer { get; set; }
        public double AccessTokenExpiration { get; set; }
        public double RefreshTokenExpiration { get; set; }
    }
}
