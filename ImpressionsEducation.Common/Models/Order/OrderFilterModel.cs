﻿using ImpressionsEducation.Common.Enums;

namespace ImpressionsEducation.Common.Models.Order
{
    public class OrderFilterModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string UserId { get; set; }
        public SortOrder SortOrder { get; set; }
        public OrderColumnName OrderColumnName { get; set; }
        public StatusOrder StatusOrder { get; set; }
    }
}
