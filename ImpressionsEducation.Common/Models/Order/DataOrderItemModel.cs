﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImpressionsEducation.Common.Models.Order
{
    public class DataOrderItemModel
    {
        public string OrderId { get; set; }
        public DateTime DateOrder { get; set; }
        public decimal OrderAmount { get; set; }
        public int OrderStatus { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public List<OrderItemData> OrderItems { get; set; }
    }

    public class OrderItemData 
    {
        public int Count { get; set; }
        public string NameImpressions { get; set; }
    }
}
