﻿using System.Collections.Generic;

namespace ImpressionsEducation.Common.Models
{
    public class PaginationModel<T> where T : class
    {
        public IEnumerable<T> Items { get; set; }
        public int Total { get; set; }
    }
}
