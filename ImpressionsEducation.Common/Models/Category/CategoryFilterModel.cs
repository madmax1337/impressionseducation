﻿using ImpressionsEducation.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionsEducation.Common.Models.Category
{
    public class CategoryFilterModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string NameCategory { get; set; }
        public SortOrder SortOrder { get; set; }
        public CategoryColumnName CategoryColumnName { get; set; }
    }
}
