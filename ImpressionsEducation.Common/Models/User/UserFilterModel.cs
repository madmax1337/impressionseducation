﻿using ImpressionsEducation.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImpressionsEducation.Common.Models.User
{
    public class UserFilterModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SearchString { get; set; }
        public bool Deleted { get; set; }
        public bool ToSort { get; set; }
        public LockoutStatus LockoutStatus { get; set; }
        public SortOrder SortOrder { get; set; }
        public UserField SortFieldUser { get; set; }
    }
}
