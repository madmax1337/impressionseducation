﻿namespace ImpressionsEducation.Common.Enums
{
    public enum LockoutStatus
    {
        Active=0,
        Blocked=1,
        All=2
    }
}
