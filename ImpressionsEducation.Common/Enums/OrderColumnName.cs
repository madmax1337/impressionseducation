﻿using System.ComponentModel;

namespace ImpressionsEducation.Common.Enums
{
    public enum OrderColumnName
    {
        [Description("OrderId")]
        OrderId = 0,
        [Description("DateOrder")]
        DateOrder = 1,
        [Description("OrderAmount")]
        OrderAmount = 2,
        [Description("OrderStatus")]
        OrderStatus = 3,
        [Description("UserName")]
        UserName = 4,
        [Description("Email")]
        Email = 5,
        [Description("OrderCount")]
        OrderCount = 6
    }
}
