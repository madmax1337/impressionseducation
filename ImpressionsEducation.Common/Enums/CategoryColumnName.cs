﻿using System.ComponentModel;

namespace ImpressionsEducation.Common.Enums
{
    public enum CategoryColumnName
    {
        [Description("CreationDate")]
        CreationDate = 0,
        [Description("NameCategory")]
        NameCategory = 1,
        [Description("Id")]
        Id = 2
    }
}
