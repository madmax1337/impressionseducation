﻿namespace ImpressionsEducation.Common.Enums
{
    public enum UserField
    {
        UserName = 0,
        Email = 1,
        FirstName = 2,
        LastName = 3,
        Password = 4
    }
}
