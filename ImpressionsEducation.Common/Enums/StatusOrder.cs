﻿namespace ImpressionsEducation.Common.Enums
{
    public enum StatusOrder
    {
        StatusConfirmed = 0,
        StatusNotConfirmed = 1,
        All = 2
    }
}
