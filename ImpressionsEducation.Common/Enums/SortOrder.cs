﻿using System.ComponentModel;

namespace ImpressionsEducation.Common.Enums
{
    public enum SortOrder
    {
        [Description("ASC")]
        Ascending = 0,

        [Description("DESC")]
        Descending = 1
    }
}
